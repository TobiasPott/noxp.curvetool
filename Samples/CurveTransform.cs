using System;
using UnityEngine;

namespace NoXP
{

    public enum CurveTransformPace
    {
        None,
        Duration,
        Speed
    }

    [Serializable()]
    public struct CurveTransformOptions
    {
        public bool Loop;
        public bool Reverse;
        public bool OrientToCurve;
        public bool FollowTransition;

        public CurveTransformOptions(bool loop, bool reverse, bool orientToCurve, bool followTransition)
        {
            Loop = loop;
            Reverse = reverse;
            OrientToCurve = orientToCurve;
            FollowTransition = followTransition;
        }

        public void SetLoop(bool loop)
        { Loop = loop; }
        public void SetReverse(bool reverse)
        { Reverse = reverse; }
        public void SetOrientToCurve(bool orientToCurve)
        { OrientToCurve = orientToCurve; }
        public void SetFollowTransition(bool followTransition)
        { FollowTransition = followTransition; }
    }


#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Samples/Curve Transform")]
#endif
    public class CurveTransform : MonoBehaviour
    {
        [SerializeField()]
        private CurveBase _curve;
        [SerializeField()]
        private CurveBase _curveUp;

        [SerializeField()]
        private CurveTransformData _data = new CurveTransformData();

        private Rigidbody _rigidbody = null;


        private void Awake()
        {
            _rigidbody = this.GetComponent<Rigidbody>();
        }

        void Start()
        {
            // set the CurveTime to 1.0 to start at the end instead of the beginning of the curve
            if (_data.Options.Reverse)
                _data.CurveTime = 1.0f;
            _data.CurveTime = _data.TimeOffset;
        }

        // Update is called once per frame
        void Update()
        {
            if (_data != null && _curve != null)
            {
                if (_rigidbody != null)
                    this._data.Evaluate(null, _rigidbody, ref this._curve);
                else
                    this._data.Evaluate(this.transform, null, ref this._curve);
            }

        }

    }

}