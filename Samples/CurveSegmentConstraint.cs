﻿using UnityEngine;
using System.Collections;
using System;

namespace NoXP
{
    [RequireComponent(typeof(Curve))]
    public class CurveSegmentConstraint : MonoBehaviour
    {
        [Serializable()]
        public class CurvePointToRigidbodyLink
        {
            public int Index;
            public Rigidbody Rigidbody;

            private Vector3 _offset;
            public Vector3 Offset
            {
                get { return _offset; }
                set { _offset = value; }
            }
        }


        private Curve _curve;
        [SerializeField()]
        private CurvePointToRigidbodyLink[] _links = new CurvePointToRigidbodyLink[0];

        void Awake()
        {
            _curve = this.GetComponent<Curve>();

            foreach (CurvePointToRigidbodyLink link in _links)
            {
                Vector3 point = Vector3.zero;
                if (link.Index < _curve.NumberOfSegments)
                    point = this.transform.TransformPoint(_curve[link.Index].Start);
                else
                    point = this.transform.TransformPoint(_curve[link.Index - 1].End);

                link.Offset = point - link.Rigidbody.position;
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            foreach (CurvePointToRigidbodyLink link in _links)
            {
                if (link.Index < 0 || link.Index > _curve.NumberOfSegments)
                    continue;

                Vector3 point = this.transform.InverseTransformPoint(link.Rigidbody.position);
                if (link.Index < _curve.NumberOfSegments)
                {
                    if (link.Index > 0)
                        _curve[link.Index - 1].End = point + link.Offset;
                    _curve[link.Index].Start = point + link.Offset;
                }
                else
                {
                    _curve[link.Index - 1].End = point + link.Offset;
                }



            }
        }


    }

}