﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace NoXP
{

    public interface ICopyable<T>
    {
        void CopyFrom(T source);
    }

    public interface ICurve
    {
        Vector3 Position(float time);
        Vector3 LocalPosition(float time);

        float Length { get; }
        bool HasTransition(ref float time, ref CurveBase curve);
    }

    public abstract class CurveBase : MonoBehaviour, ICurve
    {
        protected List<CurveTransitionInfo> _transitions = new List<CurveTransitionInfo>();

        // properties
        public abstract float Length { get; }
        public List<CurveTransitionInfo> Transitions
        { get { return _transitions; } }


        // abstract method declararions 
        public abstract Vector3 Position(float time);
        public abstract Vector3 LocalPosition(float time);
        public abstract Vector3 RawPosition(float time);
        public abstract Vector3 RawLocalPosition(float time);




        // CurveTransitionInfo handling
        public virtual bool HasTransition(ref float time, ref CurveBase curve)
        {
            foreach (CurveTransitionInfo transition in _transitions)
            {
                if (!transition.Active)
                    continue;

                if (time > transition.Time && time <= transition.Time + Time.deltaTime)
                {
                    time = transition.TargetTime; // + Time.deltaTime;
                    curve = transition.Curve;
                    return true;
                }
            }
            return false;
        }
        public void AddTransitionOut(CurveTransitionInfo transitionInfo)
        {
            if (!_transitions.Contains(transitionInfo))
                _transitions.Add(transitionInfo);
        }
        public void RemoveTransitionOut(CurveTransitionInfo transitionInfo)
        {
            if (_transitions.Contains(transitionInfo))
                _transitions.Remove(transitionInfo);
        }
        public void ClearTransitionData()
        {
            this._transitions.Clear();
        }


        // drawing gizmo icon for curve base subclasses
        protected string GizmoIconName
        { get { return string.Format("NoXP.Gizmos.{0}.png", this.GetType().Name); } }

        protected void DrawGizmoIcon(Transform transform)
        {
            Gizmos.DrawIcon(transform.position, GizmoIconName, true);
        }

    }


    [Serializable]
    public class CurveSegment : ICopyable<CurveSegment>
    {
        // icons: (possible sources
        // http://www.famfamfam.com/lab/icons/silk/

        [SerializeField()]
        private Vector3 _start;
        [SerializeField()]
        private Vector3 _startTangent;
        [SerializeField()]
        private Vector3 _end;
        [SerializeField()]
        private Vector3 _endTangent;
        [SerializeField()]
        private CurveTangentModes _endTangentMode = CurveTangentModes.Individual;
        [SerializeField()]
        private bool _isLink = false;

        public Vector3 Start
        {
            get { return _start; }
            set { _start = value; }
        }
        public Vector3 StartTangent
        {
            get { return _startTangent; }
            set { _startTangent = value; }
        }

        public Vector3 End
        {
            get { return _end; }
            set { _end = value; }
        }
        public Vector3 EndTangent
        {
            get { return _endTangent; }
            set { _endTangent = value; }
        }
        public CurveTangentModes EndTangentMode
        {
            get { return _endTangentMode; }
            set { _endTangentMode = value; }
        }
        public bool IsLink
        {
            get { return _isLink; }
            set { _isLink = value; }
        }


        public CurveSegment(Vector3 start, Vector3 startTangent, Vector3 end, Vector3 endTangent)
        {
            _start = start;
            _startTangent = startTangent;
            _end = end;
            _endTangent = endTangent;
        }
        public CurveSegment(CurveSegment source)
        { this.CopyFrom(source); }


        // ICopyable implementation
        public void CopyFrom(CurveSegment source)
        {
            _endTangentMode = source._endTangentMode;
            _start = source._start;
            _startTangent = source._startTangent;
            _end = source._end;
            _endTangent = source._endTangent;
            _isLink = source._isLink;
        }


        public static void Copy(CurveSegment source, CurveSegment target)
        {
            target.CopyFrom(source);
        }

    }


}