using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NoXP
{
    public enum CurveTypes
    {
        Bezier,
        Quadric, // not supported yet
        Linear // not supported yet
    }
    public enum CurveEditorTools
    {
        Edit,
        Add,
        Remove,
        Corner,
        Smooth,
        Pivot
    }
    public enum CurveTangentModes
    {
        Individual,
        Shared
    }
    public enum CurveSegmentParts
    {
        Start,
        End
    }
    public enum CurveSegmentAxes
    {
        X = 0,
        Y = 1,
        Z = 2
    }
    public enum CurveSegmentElements
    {
        CurvePoints = 1,
        Tangents = 2
    }
    public enum CurveSegmentVerbosity
    {
        Off,
        Minimal,
        Points,
        Tangents,
        Full
    }




#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve")]
#endif
    public class Curve : CurveBase, ICopyable<Curve>
    {
        protected CurveRetime _extRetime = null;
        protected CurveTransition _extTransition = null;
        protected CurveRemap _extRemap = null;

        [SerializeField()]
        private CurveTypes _type = CurveTypes.Bezier;

        protected float _length = 1.0f;


        private AnimationCurve _segIndicesConcise = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 0.0f));
        public AnimationCurve SegIndicesConcise
        { get { return _segIndicesConcise; } }

        private int[] _segIndicesConciseToVerbose = new int[] { 0 };

        [SerializeField()]
        private List<CurveSegment> _segments = new List<CurveSegment> { new CurveSegment(new Vector3(0, 0, 0), new Vector3(0, 0, 20), new Vector3(0, 0, 100), new Vector3(0, 0, 20)) };


        public CurveSegment this[int index]
        {
            get { return _segments[index]; }
            set { _segments[index] = value; }
        }

        /// <summary>
        /// gets the total number of curve segments
        /// </summary>
        public int NumberOfSegments
        { get { return _segments.Count; } }
        public int NumberOfSegmentsConcise
        { get { return _segments.Count(x => !x.IsLink); } }

        protected float SegmentSpan
        { get { return 1.0f / NumberOfSegments; } }
        protected float SegmentSpanConcise
        { get { return 1.0f / NumberOfSegmentsConcise; } }

        public override float Length
        {
            get
            {
                //_length = this.ApproxLength(8, false);
                return _length;
            }
        }

        public CurveTypes Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public bool IsTransition
        { get { return _extTransition != null; } }



        void Awake()
        {
            // length of verbose curve is not required outside the Curve class
            _length = this.ApproxLength(8, false);
            this.RefreshConciseData();
        }

        public void RefreshConciseData()
        {
            _length = this.ApproxLength(8, false);
            // clear the animation curve keyframes
            for (int i = _segIndicesConcise.length - 1; i >= 0; i--)
                _segIndicesConcise.RemoveKey(i);

            // get number of available non-link segments
            int numOfSegmentsConcise = this.NumberOfSegmentsConcise;
            float spanOfSegmentConcise = 1.0f / numOfSegmentsConcise;

            if (_segIndicesConciseToVerbose.Length < numOfSegmentsConcise
                || _segIndicesConciseToVerbose == null)
                _segIndicesConciseToVerbose = new int[numOfSegmentsConcise];

            int accessor = 0;

            for (int i = 0; i < this.NumberOfSegments; i++)
            {
                if (this._segments[i].IsLink)
                    continue;
                _segIndicesConciseToVerbose[accessor] = i;
                accessor++;
            }

            // calc the segment length for non-link segment data
            //_spanOfSegmentConcise = 1.0f / (numOfSegmentsConcise);
            // create segment indices curve for later lookup
            for (int i = 1; i <= numOfSegmentsConcise; i++)
            {
                _segIndicesConcise.AddKey(new Keyframe((i - 1) * spanOfSegmentConcise, _segIndicesConciseToVerbose[i - 1], 0, 0));
                if (i != numOfSegmentsConcise)
                    _segIndicesConcise.AddKey(new Keyframe(i * spanOfSegmentConcise - 0.0001f, _segIndicesConciseToVerbose[i - 1], 0, 0));
                else
                {
                    _segIndicesConcise.AddKey(new Keyframe(1.0f, _segIndicesConciseToVerbose[i - 1], 0, 0));
                    _segIndicesConcise.AddKey(new Keyframe(1.1f, _segIndicesConciseToVerbose[i - 1], 0, 0));
                }
            }

        }

        public int ConciseToVerboseIndex(int index)
        {
            this.RefreshConciseData();
            //Debug.Log(_segIndicesConciseToVerbose.Length);
            // ! ! ! !
            // array might have wrong length when a segment is marked as linked on curve
            return _segIndicesConciseToVerbose[index];
        }

        // segment handling and modification
        public void Add(CurveSegment segment)
        { this._segments.Add(segment); }
        public void AddRange(IEnumerable<CurveSegment> segments)
        { this._segments.AddRange(segments); }
        public bool Remove(CurveSegment segment)
        { return this._segments.Remove(segment); }
        public void RemoveAt(int index)
        { this._segments.RemoveAt(index); }
        public void RemoveRange(int index, int count)
        { this._segments.RemoveRange(index, count); }
        public void Clear()
        {
            this._segments.Clear();
            this._segments.Add(new CurveSegment(new Vector3(0, 0, 0), new Vector3(0, 0, 20), new Vector3(0, 0, 100), new Vector3(0, 0, 20)));
            this._length = this.ApproxLength(8, false);
            this.RefreshConciseData();
        }


        public float EvaluateSegment(float time, ref int segmentIndex, bool concise = false)
        {
            // check if concise data should be processed or verbose (default is verbose)
            if (!concise)
            {
                segmentIndex = this.SegmentIndex(time);
                return (time - (segmentIndex * SegmentSpan)) * NumberOfSegments;
            }
            else
            {
                segmentIndex = this.SegmentIndexConcise(time);
                int segNonLinkIndex = Array.IndexOf(_segIndicesConciseToVerbose, segmentIndex);
                //Debug.Log("Non-Retime SegIndex: " + segmentIndex);
                return (time - (segNonLinkIndex * SegmentSpanConcise)) * NumberOfSegmentsConcise;
            }
        }
        public int SegmentIndex(float time)
        {
            return (int)(NumberOfSegments * Mathf.Clamp(time, 0, 0.9999995f));
        }
        public int SegmentIndexConcise(float time)
        {
            return (int)(_segIndicesConcise.Evaluate(time));
        }


        public void SplitSegment(int segIndex, float segTime)
        {
            if (segTime < 0.0005f)
                this.InsertSegment(segIndex, CurveSegmentParts.Start);
            else if (segTime > 0.9995f)
                this.InsertSegment(segIndex, CurveSegmentParts.End);
            else
            {
                float halfMagStart = _segments[segIndex].EndTangent.magnitude * 0.5f;

                float splitLoc = (segIndex * this.SegmentSpan) + (segTime * this.SegmentSpan);
                Vector3 splitPos = this.RawLocalPositionVerbose(splitLoc);
                Vector3 splitTangent = -(splitPos - this.RawLocalPositionVerbose(splitLoc + 0.005f)).normalized;

                CurveSegment segment = new CurveSegment(splitPos, splitTangent * halfMagStart, _segments[segIndex].End, _segments[segIndex].EndTangent);
                segment.EndTangentMode = _segments[segIndex].EndTangentMode;
                _segments[segIndex].End = splitPos;
                _segments[segIndex].EndTangent = splitTangent * halfMagStart;
                _segments[segIndex].StartTangent = _segments[segIndex].StartTangent;
                _segments[segIndex].EndTangentMode = CurveTangentModes.Shared;
                _segments.Insert(segIndex + 1, segment);
            }

        }
        public void InsertSegment(int index, CurveSegmentParts part)
        {
            index = Mathf.Clamp(index, 0, _segments.Count - 1);

            CurveSegment segment = new CurveSegment(Vector3.zero, Vector3.forward, new Vector3(0, 0, 10), -Vector3.forward);
            segment.EndTangentMode = CurveTangentModes.Shared;
            //if (part == SegmentParts.Start)
            // check if insertion shoud be done on first or last index
            if (index == 0 && part == CurveSegmentParts.Start)
            {
                float halfMag = _segments[0].EndTangent.magnitude * 0.5f;
                segment.Start = _segments[0].Start;
                segment.StartTangent = _segments[0].StartTangent * 0.5f;
                Vector3 nEnd = Curve.Sample(_segments[0], 0.5f, this.Type);
                Vector3 nEndTangent = -(nEnd - Curve.Sample(_segments[0], 0.50001f, this.Type)).normalized * halfMag;
                segment.End = nEnd;
                segment.EndTangent = nEndTangent;
                _segments[0].Start = nEnd;
                _segments[0].StartTangent = nEndTangent;
                _segments[0].EndTangent = _segments[0].EndTangent * 0.5f;
                _segments.Insert(index, segment);
            }
            else if (index == _segments.Count - 1 && part == CurveSegmentParts.End)
            {
                if (_segments.Count == 1)
                    index = 0;
                //int indexMinusOne = Mathf.Clamp(index - 1, 0, _segments.Count - 1);
                float halfMag = _segments[index].EndTangent.magnitude * 0.5f;
                segment.End = _segments[index].End;
                segment.EndTangent = _segments[index].EndTangent * 0.5f;
                Vector3 nStart = Curve.Sample(_segments[index], 0.5f, this.Type);
                Vector3 nStartTangent = (nStart - Curve.Sample(_segments[index], 0.49999f, this.Type)).normalized * halfMag;
                segment.Start = nStart;
                segment.StartTangent = nStartTangent;
                _segments[index].End = nStart;
                _segments[index].EndTangent = nStartTangent;
                _segments[index].StartTangent = _segments[index].StartTangent * 0.5f;
                _segments.Insert(_segments.Count, segment);
            }

            else
            {
                if (part == CurveSegmentParts.End)
                    index++;
                // inserts the segment behind the given index; always index + 1
                int prevIndex = index - 1;
                float halfMagStart = _segments[prevIndex].EndTangent.magnitude * 0.5f;
                Vector3 nStart = Curve.Sample(_segments[prevIndex], 0.5f, this.Type);
                Vector3 nStartTangent = (nStart - Curve.Sample(_segments[prevIndex], 0.45f, this.Type)).normalized * halfMagStart;

                float halfMagEnd = _segments[index].EndTangent.magnitude * 0.5f;
                Vector3 nEnd = Curve.Sample(_segments[index], 0.5f, this.Type);
                Vector3 nEndTangent = -(nEnd - Curve.Sample(_segments[index], 0.55f, this.Type)).normalized * halfMagEnd;

                segment.Start = nStart;
                segment.StartTangent = nStartTangent;
                _segments[prevIndex].End = nStart;
                _segments[prevIndex].EndTangent = _segments[prevIndex].EndTangent * 0.5f;
                _segments[prevIndex].StartTangent = _segments[prevIndex].StartTangent * 0.5f;
                // set new end to the new segment (and as start values to the preceeding segment)
                segment.End = nEnd;
                segment.EndTangent = nEndTangent;
                _segments[index].Start = nEnd;
                _segments[index].StartTangent = _segments[index].StartTangent * 0.5f;
                _segments[index].EndTangent = _segments[index].EndTangent * 0.5f;
                _segments.Insert(index, segment);

            }

        }
        public void DeleteSegment(int index, CurveSegmentParts part)
        {
            // check early exit to only allow segment removal when more than one segment exists
            if (_segments.Count == 1)
                return;
            else if (part == CurveSegmentParts.Start)
            {
                if (index == 0 && this.NumberOfSegments > 1)
                    _segments.RemoveAt(index);
                else if (index > 0 && index <= _segments.Count - 1)
                {
                    // merge the segment with the previous segment
                    int mIndex = index - 1;
                    _segments[mIndex].End = _segments[index].End;
                    _segments[mIndex].EndTangent = _segments[index].EndTangent; // * (1.0f + _segments[mIndex].EndTangent.magnitude);
                    _segments.RemoveAt(index);
                }
            }
            else // remove the end part of the segment
            {
                if (index == _segments.Count - 1 && this.NumberOfSegments > 1)
                    _segments.RemoveAt(index);
                else if (index < _segments.Count - 1)
                {
                    // merge the segment with the previous segmexnt
                    int mIndex = index + 1;
                    _segments[mIndex].Start = _segments[index].Start;
                    _segments[mIndex].StartTangent = _segments[index].StartTangent; // * (1.0f + _segments[mIndex].StartTangent.magnitude);
                    _segments.RemoveAt(index);
                }
            }

        }
        public void AlignSegmentsToAxis(CurveSegmentAxes axis, CurveSegmentElements elements)
        {
            bool cp = (elements & CurveSegmentElements.CurvePoints) == CurveSegmentElements.CurvePoints;
            bool tan = (elements & CurveSegmentElements.Tangents) == CurveSegmentElements.Tangents;

            for (int i = 0; i < _segments.Count; i++)
            {
                if (cp)
                {
                    _segments[i].Start = CurveHelper.Zero(axis, _segments[i].Start);
                    _segments[i].End = CurveHelper.Zero(axis, _segments[i].End);
                }
                if (tan)
                {
                    _segments[i].StartTangent = CurveHelper.Zero(axis, _segments[i].StartTangent);
                    _segments[i].EndTangent = CurveHelper.Zero(axis, _segments[i].EndTangent);
                }
            }
        }

        public void MakeCurvePointCorner(int index, CurveSegmentParts part)
        {
            if (part == CurveSegmentParts.End)
                index++;
            // ! ! ! ! 
            // get the tangent length before modifying and reapply it to the new tangent
            if (index == 0)
                this._segments[0].StartTangent = (this._segments[0].End - this._segments[0].Start) * 0.5f;
            else if (index == _segments.Count)
                this._segments[index - 1].EndTangent = (this._segments[index - 1].End - this._segments[index - 1].Start) * 0.5f;
            else
            {
                index--;
                Vector3 inTangent = this._segments[index].End - this._segments[index].Start;
                Vector3 outTangent = this._segments[index + 1].End - this._segments[index].End;

                this._segments[index].EndTangentMode = CurveTangentModes.Individual;
                this._segments[index].EndTangent = inTangent * 0.5f;
                this._segments[index + 1].StartTangent = outTangent * 0.5f;
            }
        }
        public void MakeCurvePointSmooth(int index, CurveSegmentParts part)
        {
            if (part == CurveSegmentParts.End)
                index++;
            // ! ! ! ! 
            // get the tangent length before modifying and reapply it to the new tangent
            if (index > 0 && index < _segments.Count)
            {
                index--;
                Vector3 tangent = this._segments[index + 1].End - this._segments[index].Start;

                this._segments[index].EndTangentMode = CurveTangentModes.Shared;
                this._segments[index].EndTangent = tangent * 0.25f; // tangent was calculated over two full segments = 4 tangents (start, end, start, end)
            }
        }
        public void ReverseCompleteCurve()
        {
            CurveSegment tmp = new CurveSegment(Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero);
            for (int i = 0; i < _segments.Count; i++)
            {
                CurveSegment seg = _segments[i];
                tmp.Start = seg.Start;
                tmp.StartTangent = seg.StartTangent;
                tmp.End = seg.End;
                tmp.EndTangent = seg.EndTangent;
                // set the values back to the segment
                seg.Start = tmp.End;
                seg.StartTangent = -tmp.EndTangent;
                seg.End = tmp.Start;
                seg.EndTangent = -tmp.StartTangent;
            }
            _segments.Reverse();

        }
        public void CloseEndSegmentToStart()
        {
            // ! ! ! !
            // does not allow shared tangents between start and end point (they are not associated in data structures and would require individual code)
            if (_segments.Count <= 0)
                return;

            int firstIndex = 0;
            int lastIndex = _segments.Count - 1;

            _segments[lastIndex].End = _segments[firstIndex].Start;
            _segments[lastIndex].EndTangent = _segments[firstIndex].StartTangent;
            _segments[lastIndex].EndTangentMode = CurveTangentModes.Shared;
        }
        public void ModifyPivotTranslate(Vector3 translate, bool local)
        {
            Vector3 localTranslate = !local ? this.transform.InverseTransformVector(translate) : translate;
            for (int i = 0; i < _segments.Count; i++)
            {
                _segments[i].Start -= localTranslate;
                _segments[i].End -= localTranslate;
            }
            this.transform.position = this.transform.position + translate;
        }


#if UNITY_EDITOR
        // MonoBehaviour methods
        public void OnDrawGizmos()
        {
            if (CurveHelper.OptionDrawGizmos)
            {
                this.DrawGizmoIcon(this.transform);
                //Curve.DrawCurveGizmo(this, CurveHelper.ColorCurveGizmo, CurveHelper.ColorCurveHandles);
                //Gizmos.DrawIcon(this.transform.position, "NoXP.Curve.Gizmos.Icon.png", true);
                //int index = Array.IndexOf(Selection.gameObjects, this.gameObject);
                //if (index == -1)
                //    CurveHelper.SceneDrawCurve(this, false, Color.white);

                //// draw gizmo line (which is selectable)
                //CurveHelper.GizmosDrawCurve(this, 8); // 12 makes beautiful curves but might be a bit to much data mangling
            }
        }

        public static void DrawCurveGizmoIcon(Transform transform)
        {
            Gizmos.DrawIcon(transform.position, "NoXP.Gizmos.Curve.png", true);
        }

        public static void DrawCurveGizmo(Curve curve, Color gizmoColor, Color handleColor, Transform transform = null)
        {
            if (transform == null)
                transform = curve.transform;

            Color gizClr = Gizmos.color;
            Gizmos.color = gizmoColor;
            // draw gizmo line (which is selectable)
            CurveHelper.GizmosDrawCurve(curve, 8, transform); // 12 makes beautiful curves but might be a bit to much data mangling

            int index = Array.IndexOf(Selection.gameObjects, transform.gameObject);
            if (index == -1)
                CurveHelper.SceneDrawCurve(curve, false, handleColor, 1.0f, transform);

            Gizmos.color = gizClr;
        }

        //public void OnDrawGizmosSelected()
        //{
        //    //CurveHelper.GizmosDrawCurve(this, 8);
        //    //if (CurveHelper.OptionDrawGizmos)
        //    //CurveHelper.SceneDrawCurve(this, false, Color.white);
        //}

#endif

        // Bezier to Position wrapper
        /// <summary>
        /// gets the position on the bezier curve for the normalized time
        /// </summary>
        /// <param name="time">the normalized time (range from 0.0 to 1.0 where 0 is the start of the curve and 1.0 is the end)</param>
        /// <returns>the position as Vector3 on the BezierCurve for the spezified time</returns>
        public override Vector3 Position(float time)
        { return this.Sample(time, true); }
        public Vector3 PositionVerbose(float time)
        { return this.Sample(time, false); }
        ///// <summary>
        ///// gets the position on the bezier curve for the normalized time
        ///// </summary>
        ///// <param name="time">the normalized time (range from 0.0 to 1.0 where 0 is the start of the curve and 1.0 is the end)</param>
        ///// <returns>the position as Vector3 on the BezierCurve for the spezified time</returns>
        public override Vector3 RawPosition(float time)
        { return this.RawSample(time, true); }
        public Vector3 RawPositionVerbose(float time)
        { return this.RawSample(time, false); }
        /// <summary>
        /// gets the position on the bezier curve for the normalized time
        /// </summary>
        /// <param name="time">the normalized time (range from 0.0 to 1.0 where 0 is the start of the curve and 1.0 is the end)</param>
        /// <returns>the position as Vector3 on the BezierCurve for the spezified time</returns>
        public override Vector3 LocalPosition(float time)
        { return this.LocalSample(time, true); }
        public Vector3 LocalPositionVerbose(float time)
        { return this.LocalSample(time, false); }
        /// <summary>
        /// gets the position on the bezier curve for the normalized time
        /// </summary>
        /// <param name="time">the normalized time (range from 0.0 to 1.0 where 0 is the start of the curve and 1.0 is the end)</param>
        /// <returns>the position as Vector3 on the BezierCurve for the spezified time</returns>
        public override Vector3 RawLocalPosition(float time)
        { return this.RawLocalSample(time, true); }
        public Vector3 RawLocalPositionVerbose(float time)
        { return this.RawLocalSample(time, false); }


        private Vector3 Sample(float time, bool concise = false)
        {
            return this.transform.TransformPoint(LocalSample(time, concise));
        }
        private Vector3 LocalSample(float time, bool concise = false)
        {
            if (_extRemap != null && _extRemap.enabled)
                time = _extRemap.Evaluate(time);

            int segIndex = 0;
            float segTime = this.EvaluateSegment(time, ref segIndex, concise);

            // ! ! ! !
            // one some (very long curves the adjustment curve causes transformer to flip forward
            if (_extRetime != null && _extRetime.RetimeMode == CurveRetimeModes.Adjusted)
                segTime = _extRetime.EvaluateSegment(time, ref segIndex);

            // clamp to keep between 0 and (number of segments - 1)
            segIndex = Mathf.Clamp(segIndex, 0, _segments.Count - 1);
            return Curve.Sample(this[segIndex], segTime, this.Type);
        }

        private Vector3 RawSample(float time, bool concise = false)
        { return this.transform.TransformPoint(this.RawLocalSample(time, concise)); }
        private Vector3 RawLocalSample(float time, bool concise = false)
        {
            int segIndex = 0;
            float segTime = this.EvaluateSegment(time, ref segIndex, concise);

            return Curve.Sample(this[segIndex], segTime, this.Type);
        }

        // CurveExtension handling
        public void RegisterExtension(CurveExtension extension, bool force = true)
        {
            // check for the extension type
            if (extension.GetType().Equals(typeof(CurveRetime)))
            {
                // check if extension slot is null or if it should be forced
                if (_extRetime == null || force)
                    _extRetime = (CurveRetime)extension; // set extension to slot
            }
            else if (extension.GetType().Equals(typeof(CurveRemap)))
            {
                // check if extension slot is null or if it should be forced
                if (_extRemap == null || force)
                    _extRemap = (CurveRemap)extension; // set extension to slot
            }
            else if (extension.GetType().Equals(typeof(CurveTransition)))
            {
                // check if extension slot is null or if it should be forced
                if (_extTransition == null || force)
                    _extTransition = (CurveTransition)extension; // set extension to slot
            }
        }

        public void InitExtensions()
        {
            CurveExtension[] exts = this.gameObject.GetComponents<CurveExtension>();
            foreach (CurveExtension ext in exts)
                //if (ext.gameObject.Equals(this.gameObject))
                this.RegisterExtension(ext, true);
        }
        public void RefreshExtensions()
        {
            if (_extRetime != null)
                _extRetime.Rebuild();
            if (_extTransition != null)
                _extTransition.Refresh();
        }

        // CurveTransitionInfo handling
        //public void AddTransitionOut(CurveTransitionInfo transitionInfo)
        //{
        //    if (!_transitions.Contains(transitionInfo))
        //        _transitions.Add(transitionInfo);
        //}
        //public void RemoveTransitionOut(CurveTransitionInfo transitionInfo)
        //{
        //    if (_transitions.Contains(transitionInfo))
        //        _transitions.Remove(transitionInfo);
        //}
        //public void ClearTransitionData()
        //{
        //    this._transitions.Clear();
        //}
        //public override bool HasTransition(ref float time, ref CurveBase curve)
        //{
        //    foreach (CurveTransitionInfo transition in _transitions)
        //    {
        //        if (!transition.Active)
        //            continue;

        //        if (time > transition.Time && time <= transition.Time + Time.deltaTime)
        //        {
        //            time = transition.TargetTime; // + Time.deltaTime;
        //            curve = transition.Curve;
        //            return true;
        //        }
        //    }
        //    return false;
        //}


        // ICopyable implementation
        public void CopyFrom(Curve source)
        {
            this._type = source._type;
            this._length = source._length;
            this._segments.Clear();
            foreach (CurveSegment segment in source._segments)
                this._segments.Add(new CurveSegment(segment));
        }




        // specific helper methods
        public static void GenerateAdjustmentCurve(Curve curve, out AnimationCurve adjustCurve, out float length, List<float> segmentStarts, List<float> segmentEnds, int samples, bool smooth)
        {
            // init the 'out' parameter variables
            adjustCurve = new AnimationCurve();
            length = 0.0f;
            // clear start and end lists parameter
            segmentStarts.Clear();
            segmentEnds.Clear();

            List<int> smoothedIndices = new List<int>();

            float lastSegmentLength = 0.0f;

            // calc the duration each step covers on the segment
            float curveStepDuration = (curve.SegmentSpan) / (samples);
            float stepTimespan = 1.0f / (samples);

            for (int segment = 0; segment < curve.NumberOfSegments; segment++)
            {
                if (curve[segment].IsLink)
                    continue;

                // calc the start time for the selected segment (when processing specific segment inside the curve)
                float curSegStart = curve.SegmentSpan * segment;

                // get the initial point for the segment
                Vector3 p1 = curve.RawLocalSample(curSegStart);
                Vector3 p2; // define the second point

                p1 = (Curve.Sample(curve[segment], 0.0f, curve.Type));

                // add initial keyframe for 0.0f value at the start of the next segment
                adjustCurve.AddKey(new Keyframe(length, 0.0f, float.PositiveInfinity, 1.0f));

                for (int i = 1; i < samples; i++)
                {
                    // get the next sampled point on the bezier curve
                    p2 = curve.RawLocalSample(curSegStart + i * curveStepDuration);
                    // add distance between p1 and p2 to result
                    length += Vector3.Distance(p1, p2);

                    // check if i is not zero
                    // and add all indices of the keyframes to the list which are not the first keyframes of a segment
                    // !! => makes processing of smoothing later on more efficient
                    if (smooth && i != 0)
                        smoothedIndices.Add(adjustCurve.keys.Length);

                    adjustCurve.AddKey(new Keyframe(length - 0.0001f, i * stepTimespan, 0.0f, 0.0f));
                    // set p1 for next iteration to current p2
                    p1 = p2;
                }

                // calculate the distance between the set points
                length += Vector3.Distance(p1, curve.RawLocalSample(curSegStart + curve.SegmentSpan));

                // add the end keyframe for the segment, slightly moved left (x-axis) to prevent overriding it by the first keyframe of the next segment
                adjustCurve.AddKey(new Keyframe(length - 0.0001f, 1.0f, float.NegativeInfinity, float.PositiveInfinity));

                // add the length of the segment
                segmentStarts.Add(length - lastSegmentLength); // add it to the segmentStarts list
                                                               // store current length for next segment iteration (to have the reference start time)

                lastSegmentLength = length;
            }

            float segTimeStart = 0.0f;
            for (int i = 0; i < segmentStarts.Count; i++)
            {
                segmentEnds.Add(segTimeStart + (segmentStarts[i] / length));
                segmentStarts[i] = segTimeStart;
                segTimeStart = segmentEnds[i];
            }

            // process all keys for linear interpolation
            for (int i = 0; i < adjustCurve.keys.Length; ++i)
            {
                Keyframe key = adjustCurve[i];

                Vector2 ptCur = new Vector2(adjustCurve.keys[i].time, adjustCurve.keys[i].value);
                Vector2 ptRef, delta;
                // set all in tangents when index is not zero
                if (i > 0)
                {
                    ptRef = new Vector2(adjustCurve.keys[i - 1].time, adjustCurve.keys[i - 1].value);
                    delta = ptCur - ptRef;
                    key.inTangent = delta.y / delta.x;
                }
                // set all out tangents if index is below array length
                if (i < adjustCurve.keys.Length - 1)
                {
                    ptRef = new Vector2(adjustCurve.keys[i + 1].time, adjustCurve.keys[i + 1].value);
                    delta = ptRef - ptCur;
                    key.outTangent = delta.y / delta.x;
                }

                adjustCurve.MoveKey(i, key);
            }

            // optional smoothing of the curve (might not make any sense to NOT use it!!)
            if (smooth)
                for (int i = 0; i < smoothedIndices.Count; i++)
                    adjustCurve.SmoothTangents(smoothedIndices[i], 0.0f);

        }

        // ! ! ! 
        // kept for legacy code reasons
        /*
        [Obsolete("GenerateAdjustmentCurveVerbose is never used and marked deprecated.")]
        public static void GenerateAdjustmentCurveVerbose(Curve curve, out AnimationCurve adjustCurve, out float length, List<float> segmentStarts, List<float> segmentEnds, int samples, bool smooth)
        {

            // init the 'out' parameter variables
            adjustCurve = new AnimationCurve();
            length = 0.0f;
            // clear start and end lists parameter
            segmentStarts.Clear();
            segmentEnds.Clear();

            List<int> smoothedIndices = new List<int>();

            float lastSegmentLength = 0.0f;

            // calc the duration each step covers on the segment
            float curveStepDuration = (curve.SegmentSpan) / (samples);
            float stepTimespan = 1.0f / (samples);

            for (int segment = 0; segment < curve.NumberOfSegments; segment++)
            {
                // calc the start time for the selected segment (when processing specific segment inside the curve)
                float curSegStart = curve.SegmentSpan * segment;

                // get the initial point for the segment
                Vector3 p1 = curve.RawSample(curSegStart);
                Vector3 p2; // define the second point

                for (int i = 0; i < samples; i++)
                {
                    // get the next sampled point on the bezier curve
                    p2 = curve.RawSample(curSegStart + i * curveStepDuration);
                    // add distance between p1 and p2 to result
                    length += Vector3.Distance(p1, p2);

                    // check if i is not zero
                    // and add all indices of the keyframes to the list which are not the first keyframes of a segment
                    // !! => makes processing of smoothing later on more efficient
                    if (smooth && i != 0)
                        smoothedIndices.Add(adjustCurve.keys.Length);

                    //curve.AddKey(new Keyframe(distances[distIndexOffset + i], i * stepTimespan, 0.0f, 0.0f));
                    adjustCurve.AddKey(new Keyframe(length, i * stepTimespan, 0.0f, 0.0f));
                    // set p1 for next iteration to current p2
                    p1 = p2;
                }

                // get the second point (last point on curve segment)
                p2 = curve.RawSample(curSegStart + curve.SegmentSpan);
                // calculate the distance between the set points
                length += Vector3.Distance(p1, p2);
                //distances.Add(length);

                // add the end keyframe for the segment, slightly moved left (x-axis) to prevent overriding it by the first keyframe of the next segment
                adjustCurve.AddKey(new Keyframe(length - 0.00001f, 1.0f, float.NegativeInfinity, float.PositiveInfinity));

                // add the length of the segment
                segmentStarts.Add(length - lastSegmentLength); // add it to the segmentStarts list
                                                               // store current length for next segment iteration (to have the reference start time)
                lastSegmentLength = length;
            }

            float segTimeStart = 0.0f;
            for (int i = 0; i < segmentStarts.Count; i++)
            {
                segmentEnds.Add(segTimeStart + (segmentStarts[i] / length));
                segmentStarts[i] = segTimeStart;
                segTimeStart = segmentEnds[i];
            }

            // process all keys for linear interpolation
            for (int i = 0; i < adjustCurve.keys.Length; ++i)
            {
                Keyframe key = adjustCurve[i];

                Vector2 ptCur = new Vector2(adjustCurve.keys[i].time, adjustCurve.keys[i].value);
                Vector2 ptRef, delta;
                // set all in tangents when index is not zero
                if (i > 0)
                {
                    ptRef = new Vector2(adjustCurve.keys[i - 1].time, adjustCurve.keys[i - 1].value);
                    delta = ptCur - ptRef;
                    key.inTangent = delta.y / delta.x;
                }
                // set all out tangents if index is below array length
                if (i < adjustCurve.keys.Length - 1)
                {
                    ptRef = new Vector2(adjustCurve.keys[i + 1].time, adjustCurve.keys[i + 1].value);
                    delta = ptRef - ptCur;
                    key.outTangent = delta.y / delta.x;
                }

                adjustCurve.MoveKey(i, key);
            }
            // optional smoothing of the curve (might not make any sense to NOT use it!!)
            if (smooth)
                for (int i = 0; i < smoothedIndices.Count; i++)
                    adjustCurve.SmoothTangents(smoothedIndices[i], 0.0f);

        }
        */

        /// <summary>
        /// calculates a position on a 3D bezier curve defined by the given start/end positions and tangents
        /// <param name="s">start position of the bezier curve</param>
        /// <param name="st">start tangent of the bezier curve</param>
        /// <param name="e">end position of the bezier curve</param>
        /// <param name="et">end tangent of the bezier curve</param>
        /// <param name="t">time on the bezier (relative to the beziers length, should be a value between 0.0f and 1.0f)</param>
        /// <returns>the position in world space for the given time on the specified bezier</returns>
        public static Vector3 Bezier3(Vector3 s, Vector3 st, Vector3 e, Vector3 et, float t)
        {
            return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
        }
        public static Vector3 Bezier3(CurveSegment segment, float t)
        {
            return Curve.Bezier3(segment.Start, segment.Start + segment.StartTangent, segment.End, segment.End - segment.EndTangent, t);
        }

        public static Vector3 Linear(Vector3 s, Vector3 e, float t)
        {
            return s + (e - s) * t;
        }
        public static Vector3 Linear(CurveSegment segment, float t)
        {
            return Linear(segment.Start, segment.End, t);
        }


        public static Vector3 Sample(CurveSegment segment, float t, CurveTypes type)
        {
            if (type == CurveTypes.Bezier)
                return Bezier3(segment, t);
            else
                return Linear(segment, t);

        }

        public static void Copy(Curve source, Curve target)
        {
            target.CopyFrom(source);
        }

        public float ApproxLength(int samplesPerSegment, bool total = true)
        {
            float result = 0.0f;
            float segStep = 1.0f / (this.NumberOfSegments);
            float spanStep = segStep / samplesPerSegment;

            Vector3 p1 = RawLocalSample(0.0f);
            Vector3 p2 = p1;
            for (int s = 0; s < this.NumberOfSegments; s++)
            {
                if (!total && _segments[s].IsLink)
                    continue;

                for (int i = 1; i <= samplesPerSegment; i++)
                {
                    float time = (s * segStep) + (i * spanStep);
                    // get p2 using the curveStep and the current step number
                    p2 = RawLocalSample(time);
                    // add distance between p1 and p2 to result
                    result += Vector3.Distance(p1, p2);
                    // set p1 for next iteration to current p2
                    p1 = p2;
                }
            }
            return result;
        }

        /*
        public float ApproxLengthTotal(int samplesPerSegment)
        {
            float result = 0.0f;
            float segStep = 1.0f / (this.NumberOfSegments);
            float spanStep = segStep / samplesPerSegment;

            Vector3 p1 = RawSample(0.0f);
            Vector3 p2 = p1;
            for (int s = 0; s < this.NumberOfSegments; s++)
            {
                for (int i = 1; i <= samplesPerSegment; i++)
                {
                    float time = (s * segStep) + (i * spanStep);
                    // get p2 using the curveStep and the current step number
                    p2 = RawSample(time);
                    // add distance between p1 and p2 to result
                    result += Vector3.Distance(p1, p2);
                    // set p1 for next iteration to current p2
                    p1 = p2;
                }
            }
            return result;
        }
        */

        /*
        [Obsolete("ApproxSegmentLength(int, int)-method is never used and thus marked for removal.")]
        public float ApproxSegmentLength(int segment, int steps = 8)
        {
            if (segment >= NumberOfSegments)
                return 0.0f;

            float segStart = this.SegmentSpan * segment;

            float result = 0.0f;
            float curveStep = (this.SegmentSpan) / steps;
            Vector3 p1 = RawSample(segStart);
            Vector3 p2 = p1;
            for (int i = 1; i <= steps; i++)
            {
                // get p2 using the curveStep and the current step number
                p2 = RawSample(segStart + i * curveStep);
                // add distance between p1 and p2 to result
                result += Vector3.Distance(p1, p2);
                // set p1 for next iteration to current p2
                p1 = p2;
            }

            return result;
        }
        */

    }

}