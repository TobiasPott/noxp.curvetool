﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NoXP
{

    [Serializable()]
    public class CurveTransitionInfo
    {
        public bool Active = true;
        public float Time = 0.0f;
        public float TargetTime = 0.0f;
        public CurveBase Curve = null;


        public CurveTransitionInfo()
        { }

        public CurveTransitionInfo(float time, float targetTime, CurveBase curve)
        {
            this.Time = time;
            this.TargetTime = targetTime;
            this.Curve = curve;
        }

        public void Set(bool active, float time, float targetTime, CurveBase curve)
        {
            this.Active = active;
            this.Time = time;
            this.TargetTime = targetTime;
            this.Curve = curve;
        }

    }


#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Transition")]
#endif
    //[Serializable]
    public class CurveTransition : CurveExtension
    {
        [SerializeField()]
        protected bool _sourceActive = true;
        [SerializeField()]
        protected bool _targetActive = true;

        [Range(0, 1)]
        [SerializeField()]
        protected float _sourceTime = 1.0f;

        [Range(0, 1)]
        [SerializeField()]
        protected float _targetTime = 0.0f;

        [SerializeField()]
        protected CurveBase _source = null; // value that determines in which range of _begin a value can be to be handled as matching _begin
        [SerializeField()]
        protected CurveBase _target = null; // value that determines in which range of _begin a value can be to be handled as matching _begin

        private CurveTransitionInfo _sourceTransition = new CurveTransitionInfo();
        private CurveTransitionInfo _targetTransition = new CurveTransitionInfo();

        public bool SourceActive
        {
            get { return _sourceActive; }
            set
            {
                _sourceActive = value;
                _sourceTransition.Active = value && this.isActiveAndEnabled;
            }
        }
        public bool TargetActive
        {
            get { return _targetActive; }
            set
            {
                _targetActive = value;
                _targetTransition.Active = value && this.isActiveAndEnabled;
            }
        }

        public float SourceTime
        {
            get { return _sourceTime; }
            set
            {
                _sourceTime = value;
                _sourceTransition.Time = value;
            }
        }
        public float TargetTime
        {
            get { return _targetTime; }
            set
            {
                _targetTime = value;
                _targetTransition.TargetTime = value;
            }
        }

        public CurveBase Source
        {
            get { return _source; }
            set { this.SetSource(value); }
        }
        public CurveBase Target
        {
            get { return _target; }
            set { this.SetTarget(value); }
        }


        protected override void Awake()
        {
            base.Awake();

        }

        void Start()
        {
            this.AddToSource();
            this.AddToTarget();
            this.Refresh();
        }


        void OnDestroy()
        {
            this.RemoveFromSource();
            this.RemoveFromTarget();
        }
        void OnEnable()
        {
            this.Refresh();
        }

        void OnDisable()
        {
            this.Refresh();
        }


        public void Refresh()
        {
            if (_sourceTransition != null)
                this._sourceTransition.Set(this.SourceActive && this.isActiveAndEnabled, this.SourceTime, 0.0f, this._sourceTransition.Curve);
            if (_targetTransition != null)
                this._targetTransition.Set(this.TargetActive && this.isActiveAndEnabled, 1.0f, this.TargetTime, this._targetTransition.Curve);

            if (this.Curve != null && this.Source != null)
                this.Curve[0].Start = this.Curve.transform.InverseTransformPoint(this.Source.Position(this.SourceTime));
            if (this.Curve != null && this.Target != null)
                this.Curve[this.Curve.NumberOfSegments - 1].End = this.Curve.transform.InverseTransformPoint(this.Target.Position(this.TargetTime));
        }

        protected void RemoveFromSource()
        {
            if (this.Source != null)
                this.Source.RemoveTransitionOut(_sourceTransition); // out from source curve
        }
        protected void AddToSource()
        {
            if (this.Source != null)
            {
                _sourceTransition.Set(this.SourceActive && this.isActiveAndEnabled, this.SourceTime, 0.0f, this.Curve);
                this.Source.AddTransitionOut(_sourceTransition); // out from source curve
            }
        }
        protected void SetSource(CurveBase source)
        {
            this.RemoveFromSource();
            this._source = source;
            this.AddToSource();
        }

        protected void RemoveFromTarget()
        {
            if (this.Target != null)
                this.Curve.RemoveTransitionOut(_targetTransition); // out from transition curve
        }
        protected void AddToTarget()
        {
            if (this.Target != null)
            {
                _targetTransition.Set(this.TargetActive && this.isActiveAndEnabled, 1.0f, this.TargetTime, this.Target);
                this.Curve.AddTransitionOut(_targetTransition); // out from transition curve
            }
        }
        protected void SetTarget(CurveBase target)
        {
            this.RemoveFromTarget();
            this._target = target;
            this.AddToSource();
        }


        public static void CreateCurveTransition(CurveBase source, CurveBase target)
        {
            //Debug.Log(source + "|" + target);
            if (source == null || target == null)
                return;

            GameObject goTransition = new GameObject(source.name + "->" + target.name, typeof(Curve), typeof(CurveTransition));
            goTransition.transform.parent = source.transform.parent;
            goTransition.transform.SetSiblingIndex(source.transform.GetSiblingIndex() + 1);
            Vector3 sourcePos = source.Position(0.5f);
            Vector3 targetPos = target.Position(0.5f);
            Vector3 sourceToTarget = targetPos - sourcePos;
            goTransition.transform.position = ((sourcePos + targetPos) * 0.5f);

            CurveTransition curveTransition = goTransition.GetComponent<CurveTransition>();
            curveTransition.SetSource(source);
            curveTransition.SourceTime = 0.5f;
            curveTransition.SetTarget(target);
            curveTransition.TargetTime = 0.5f;

            // align tangents along curve (point to opposite curve points)
            Curve curve = goTransition.GetComponent<Curve>();
            curve[0].EndTangentMode = CurveTangentModes.Shared;
            curve[0].StartTangent = sourceToTarget * 0.1f;
            curve[0].EndTangent = sourceToTarget * 0.1f;

            CurveHelper.RefreshCurveTransition(curveTransition, true, true, true);
            //curveTransition.Refresh();

#if UNITY_EDITOR
            Selection.activeGameObject = goTransition;
            Undo.RegisterCreatedObjectUndo(goTransition, "Create Curve Transition");
#endif
        }

    }

}