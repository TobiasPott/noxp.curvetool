﻿using System.Collections.Generic;
using UnityEngine;

namespace NoXP
{

#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Retime")]
#endif
    [RequireComponent(typeof(Curve))]
    public abstract class CurveExtension : MonoBehaviour
    {
        protected Curve _curve;
        public Curve Curve
        {
            get
            {
                if (_curve == null) _curve = this.GetComponent<Curve>();
                return _curve;
            }
        }

        protected virtual void Awake()
        {
            _curve = this.GetComponent<Curve>();
        }
    }

    public enum CurveRetimeModes
    {
        None, // no retime applied to the evaluation
        Adjusted, // new adjusment curve based
    }
    public enum CurveRetimeQuality
    {
        Editor = 10,
        Low = 30, // low sample rate of 30 (might cause value errors during sampling a bezier curve)
        Medium = 60, // medium sample rate of 60 (works ok for most simple curve segments)
        High = 120, // high sample rate of 120 (provides better quality but might impact editor-performance)
        UltraHigh = 180 // very high sample rate of 180 (should be used when high sample rate is not precise enough)
        // perhaps add an adaptive sampling taking the individual segment length into account 
        //  ->  (which will be a two-pass process as it needs to presample all segments with a medium/high rate and determine the actual samples for each segment in relation to each segments length
    }

#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Retime")]
#endif
    [DisallowMultipleComponent]
    public class CurveRetime : CurveExtension
    {
        [SerializeField()]
        private CurveRetimeModes _retimeMode = CurveRetimeModes.None;
        [SerializeField()]
        private CurveRetimeQuality _quality = CurveRetimeQuality.Low;
        [SerializeField()]
        private List<float> _segmentsStart = new List<float>();
        [SerializeField()]
        private List<float> _segmentsEnd = new List<float>();
        [SerializeField()]
        private List<float> _segmentsSpan = new List<float>();
        [SerializeField()]
        private AnimationCurve _adjustmentCurve = new AnimationCurve(new Keyframe(0.0f, 0.0f, 0.0f, 1.0f), new Keyframe(1.0f, 1.0f, 1.0f, 0.0f));

        // store the total length of the curve
        [HideInInspector()]
        [SerializeField()]
        private float _totalCurveLength = 1.0f;

        public CurveRetimeModes RetimeMode
        {
            get { return _retimeMode; }
            set { _retimeMode = value; }
        }
        public CurveRetimeQuality Quality
        {
            get { return _quality; }
            set { _quality = value; }
        }
        public AnimationCurve AdjustmentCurve
        { get { return _adjustmentCurve; } }


        protected override void Awake()
        {
            base.Awake();
            this.Curve.RegisterExtension(this);
            this.Rebuild();
        }


        //void Start()
        //{
        //    if (_adjustmentCurve == null)
        //    {
        //        _adjustmentCurve = CurveRetime.DefaultCurve;
        //        _totalCurveLength = 1.0f;
        //    }
        //}


        public void Rebuild()
        {
            //this.Curve.RefreshConciseData();
            if (_retimeMode == CurveRetimeModes.Adjusted)
                this.BuildAdjusted();
            else
                this.BuildStandard();

            //switch (_retimeMode)
            //{
            //    case CurveRetimeModes.Adjusted:
            //        this.BuildAdjusted();
            //        break;

            //default:
            //    this.BuildStandard();
            //    _adjustmentCurve = CurveRetime.DefaultCurve;
            //    _totalCurveLength = 1.0f;
            //    break;
        }

        //private void BuildStandard()
        //{
        //    // actually only clears all lists and curves to reduce memory usage
        //    for (int i = 0; i < _adjustmentCurve.length; i++)
        //        _adjustmentCurve.RemoveKey(i);
        //    _adjustmentCurve.AddKey(new Keyframe(0.0f, 0.0f, 0.0f, 1.0f));
        //    _adjustmentCurve.AddKey(new Keyframe(1.0f, 1.0f, 1.0f, 0.0f));

        //    _segmentsStart.Clear();
        //    _segmentsEnd.Clear();
        //    _segmentsSpan.Clear();
        //}
        private void BuildStandard()
        {
            for (int i = _adjustmentCurve.length - 1; i >= 0; i--)
                _adjustmentCurve.RemoveKey(i);

            _adjustmentCurve.AddKey(new Keyframe(0.0f, 0.0f, 0.0f, 1.0f));
            _adjustmentCurve.AddKey(new Keyframe(1.0f, 1.0f, 1.0f, 0.0f));

            // ! ! ! !
            // the standard build does not work correctly when link segments are used!!
            _segmentsSpan.Clear();
            float segSpan = 1.0f / this.Curve.NumberOfSegmentsConcise;
            for (int i = 0; i < this.Curve.NumberOfSegmentsConcise; i++)
                _segmentsSpan.Add(segSpan);

            // rebuild the start and end lists
            // clear lists prior to rebuild
            _segmentsStart.Clear();
            _segmentsEnd.Clear();

            for (int i = 0; i < this._segmentsSpan.Count; i++)
            {
                float segStart = 0.0f;
                float segEnd = this._segmentsSpan[i];

                if (i != 0)
                {
                    segStart = _segmentsEnd[i - 1];
                    segEnd += _segmentsEnd[i - 1];
                }
                _segmentsStart.Add(segStart);
                _segmentsEnd.Add(segEnd);
            }
        }
        private void BuildAdjusted()
        {
            //Curve.GenerateAdjustmentCurveVerbose(this.Curve, out _adjustmentCurve, out _totalCurveLength, _segmentsStart, _segmentsEnd, (int)_quality, true);
            Curve.GenerateAdjustmentCurve(this.Curve, out _adjustmentCurve, out _totalCurveLength, _segmentsStart, _segmentsEnd, (int)_quality, false);

        }

        public float EvaluateSegment(float time, ref int segmentIndex)
        {
            if (_retimeMode == CurveRetimeModes.Adjusted)
            {
                segmentIndex = this.SegmentIndex(time);
                float result = _adjustmentCurve.Evaluate(time * _totalCurveLength);
                //return _adjustmentCurve.Evaluate(time * _totalCurveLength);
                //return _adjustmentCurve.Evaluate(time * Curve.Length);
                return Mathf.Clamp(result, 0.0f, 1.0f);
            }
            else
            {
                segmentIndex = this.SegmentIndex(time);
                Debug.Log("Returning segment time with calc not understandable");
                return (time - this.SegmentStart(segmentIndex)) * (1.0f / this.SegmentSpan(segmentIndex));
            }
        }

        public int SegmentIndex(float time)
        {
            //for (int i = 0; i < _segmentsEnd.Count; i++)
            //{
            //    if (time >= _segmentsStart[i] && time < _segmentsEnd[i])
            //    // _curve might not be initialized
            //    {
            //        return this.Curve.ConciseToVerboseIndex(i);
            //    }
            //}

            for (int i = 0; i < _segmentsEnd.Count; i++)
            {
                if (time >= _segmentsStart[i] && time < _segmentsEnd[i])
                // _curve might not be initialized
                {
                    return this.Curve.ConciseToVerboseIndex(i);
                }
            }
            return 0; // return the first segment when no other matched
        }
        private float SegmentStart(int segment)
        {
            if (_segmentsStart.Count != 0)
            {
                segment = Mathf.Clamp(segment, 0, _segmentsStart.Count - 1);
                return _segmentsStart[segment];
            }
            return 0.0f;
        }
        private float SegmentSpan(int segment)
        {
            if (_segmentsSpan.Count != 0)
            {
                segment = Mathf.Clamp(segment, 0, _segmentsSpan.Count - 1);
                return _segmentsSpan[segment];
            }
            return 0.0f;
        }



    }

}