﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NoXP
{

#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Instance")]
#endif
    public class CurveInstance : CurveBase
    {

        [SerializeField()]
        private CurveBase _curve = null;

        public CurveBase Curve
        {
            get { return _curve; }
            set { _curve = value; }
        }

        // ! ! ! ! !
        // check Length, Position, LocalPosition implementations to return correct values for the instanced curve
        public override float Length
        { get { return _curve.Length; } }


        public override Vector3 Position(float time)
        {
            if (_curve != null)
                return this.transform.TransformPoint(_curve.LocalPosition(time));
            else
                return this.transform.position;
        }
        public override Vector3 RawPosition(float time)
        {
            if (_curve != null)
                return this.transform.TransformPoint(_curve.RawLocalPosition(time));
            else
                return this.transform.position;
        }
        public override Vector3 LocalPosition(float time)
        {
            if (_curve != null)
                return _curve.LocalPosition(time);
            else
                return Vector3.zero;
        }
        public override Vector3 RawLocalPosition(float time)
        {
            if (_curve != null)
                return _curve.RawLocalPosition(time);
            else
                return Vector3.zero;
        }


        public static void Deinstantiate(CurveInstance instance)
        {
            if (instance._curve != null)
            {
                Type cType = instance._curve.GetType();

                if (cType.Equals(typeof(Curve)))
                {
                    instance.name.Replace("-Instance", "");
                    Curve curve = (Curve)instance._curve;
#if UNITY_EDITOR
                    Curve newCurve = Undo.AddComponent<Curve>(instance.gameObject);
                    NoXP.Curve.Copy(curve, newCurve);
                    GameObject.DestroyImmediate(instance);
#else
                    Curve newCurve = instance.gameObject.AddComponent<Curve>();
                    NoXP.Curve.Copy(curve, newCurve);
                    GameObject.Destroy(instance);
#endif
                }

            }

        }
        public static GameObject Instantiate(CurveBase curve)
        {
            if (curve != null)
            {
                GameObject goCurveInst = new GameObject(curve.name + "-Instance", typeof(CurveInstance));
                CurveInstance curveInst = goCurveInst.GetComponent<CurveInstance>();
                curveInst.Curve = curve;
                goCurveInst.transform.parent = curve.transform.parent;
                goCurveInst.transform.SetSiblingIndex(curve.transform.GetSiblingIndex() + 1);
                goCurveInst.transform.localPosition = curve.transform.localPosition;
                goCurveInst.transform.localRotation = curve.transform.localRotation;
                goCurveInst.transform.localScale = curve.transform.localScale;

                return goCurveInst;
            }
            return null;
        }


#if UNITY_EDITOR
        // MonoBehaviour methods
        public void OnDrawGizmos()
        {
            if (CurveHelper.OptionDrawGizmos)
            {
                if (this.Curve != null && this.Curve.GetType().Equals(typeof(Curve)))
                {
                    Curve curve = (Curve)this.Curve;

                    this.DrawGizmoIcon(this.transform);
                    NoXP.Curve.DrawCurveGizmo(curve, CurveHelper.ColorCurveInstanceGizmo, CurveHelper.ColorCurveInstanceHandles, this.transform);
                }
            }
        }
#endif

    }

}
