﻿using UnityEngine;
using System.Collections;
using System;

namespace NoXP
{

#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Blend")]
#endif
    public class CurveBlend : CurveBase
    {
        [SerializeField()]
        private CurveBase _curve0 = null;
        [SerializeField()]
        private CurveBase _curve1 = null;

        [Range(0, 1)]
        [SerializeField()]
        private float _blend = 0.0f;

        void Start()
        {
            if (_curve0 == null || _curve1 == null
                || _curve0 == this || _curve1 == this)
            {
                this.enabled = false;
                Debug.LogWarning("CurveBlend requires both curves to be set and none of it be a reference to itself! Disabled the component.");
            }
        }

        public override Vector3 LocalPosition(float time)
        {
            return Vector3.Lerp(_curve0.LocalPosition(time), _curve1.LocalPosition(time), _blend);
        }
        public override Vector3 RawLocalPosition(float time)
        {
            return Vector3.Lerp(_curve0.RawLocalPosition(time), _curve1.RawLocalPosition(time), _blend);
        }

        public override Vector3 Position(float time)
        {
            return Vector3.Lerp(_curve0.Position(time), _curve1.Position(time), _blend);
        }
        public override Vector3 RawPosition(float time)
        {
            return Vector3.Lerp(_curve0.RawPosition(time), _curve1.RawPosition(time), _blend);
        }


        public override float Length
        {
            get
            {
                return Mathf.Lerp(_curve0.Length, _curve1.Length, _blend);
            }
        }

        //public override bool HasTransition(ref float time, ref CurveBase curve)
        //{
        //    float inTime0 = time;
        //    float inTime1 = time;
        //    CurveBase c0 = _curve0;
        //    CurveBase c1 = _curve1;
        //    bool transition0 = _curve0.HasTransition(ref inTime0, ref c0);
        //    bool transition1 = _curve1.HasTransition(ref inTime1, ref c1);

        //    if (transition0 && transition1)
        //    {
        //        if (inTime0 <= inTime1)
        //            curve = c0;
        //        else
        //            curve = c1;
        //    }
        //    else if (transition0)
        //    {
        //        time = inTime0;
        //        curve = c0;
        //    }
        //    else if (transition1)
        //    {
        //        time = inTime1;
        //        curve = c1;
        //    }
        //    return transition0 || transition1;

        //}

    }

}