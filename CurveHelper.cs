﻿using UnityEngine;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NoXP
{
    public class CurveHelper
    {
        public enum CCVectors
        {
            VP2W00 = 0, // viewport to world lower left     (0,0)
            VP2W10 = 1, // viewport to world lower right    (1,0)
            VP2W01 = 2, // viewport to world upper left     (0,1)
            WRight = 3, // world right vector (from camera view)
            WUp = 4,    // world up vector (from camera view)
            VPMouse = 5 // mouse position in viewport space
        }

        private static CurveCreationParameters _ccParam = CurveCreationParameters.Default();
        public struct CurveCreationParameters
        {

            private Vector3[] _vectors; // = new Vector3[6];
            private Curve _curve; // = null;
            private bool _init; // = false;
            private int _segments; // = 1;

            public Vector3[] Vectors { get { return _vectors; } }
            public Curve Curve
            {
                get { return _curve; }
                set { _curve = value; }
            }
            public bool Init
            {
                get { return _init; }
                set { _init = value; }
            }
            public int Segments
            {
                get { return _segments; }
                set { _segments = value; }
            }


            public void Reset()
            {
                if (_vectors == null)
                    _vectors = new Vector3[6];
                _curve = null;
                _init = false;
                _segments = 1;
            }

            public static CurveCreationParameters Default()
            {
                CurveCreationParameters ccp = new CurveCreationParameters();
                ccp._vectors = new Vector3[6];
                ccp._segments = 1;
                ccp._init = false;
                return ccp;
            }

        }


        public class IMGUIWindow
        {
            #region Fields
            private string _title;
            private int _id = 3237;
            private Rect _position = new Rect(0, 0, 200, 100);
            private Action<Rect> _function = null;
            private bool _isDraggable = true;
            private bool _isVisible = true;

            private Rect _dragArea = new Rect(0, 0, 200, 16);

            #endregion

            #region Properties
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }
            public int Id
            {
                get { return _id; }
                set { _id = value; }
            }
            public Rect Position
            {
                get { return _position; }
                set { _position = value; }
            }
            public float X
            {
                get { return _position.x; }
                set { _position.x = value; }
            }
            public float YMin
            {
                get { return _position.y; }
                set { _position.y = value; }
            }
            public float Width
            {
                get { return _position.width; }
                set { _position.width = value; }
            }
            public float Height
            {
                get { return _position.height; }
                set { _position.height = value; }
            }

            public Action<Rect> Function
            {
                get { return _function; }
                set { _function = value; }
            }
            public bool IsVisible
            {
                get { return _isVisible; }
                set { _isVisible = value; }
            }

            #endregion

            public IMGUIWindow(int id, string title, Rect position, Action<Rect> function, bool visible = true)
            {
                _id = id;
                _title = title;
                _position = position;
                _function = function;
                _dragArea = new Rect(0, 0, _position.width, 16);
                _isVisible = visible;
            }



            public void SetX(float value, float pivot = 0.0f)
            { _position.x = value - _position.width * pivot; }
            public void SetY(float value, float pivot = 0.0f)
            { _position.y = value - _position.height * pivot; }
            public void SetXAndY(Vector2 pos, Vector2 pivot)
            {
                this.SetX(pos.x, pivot.x);
                this.SetY(pos.y, pivot.y);
            }

            protected virtual void WindowFunction(int windowID)
            {
                if (_function != null)
                    _function.Invoke(new Rect(0, 16, _position.width, _position.height - 16)); // content rect excludes the upper 20px (which is the title
                if (_isDraggable)
                    GUI.DragWindow(_dragArea);
            }

            public void Draw()
            {
                if (_isVisible)
                    _position = GUI.Window(_id, _position, this.WindowFunction, _title);
            }


        }


        // pseudo constants
        #region Constants
        public static readonly Color ColorTransparent = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        public static readonly Color ColorBlack = new Color(0.0f, 0.0f, 0.0f);
        public static readonly Color ColorWhite = new Color(1.0f, 1.0f, 1.0f);

        public static readonly Color ColorTangent = new Color(0.154f, 0.501f, 0.154f);
        public static readonly Color ColorTangentHandle = new Color(0.0f, 0.5f, 1.0f, 0.75f);

        public static readonly Color ColorCurve = new Color(1.0f, 0.5f, 0.0f, 0.5f); // new Color(0.309f, 0.501f, 1.0f);
        public static readonly Color ColorCurveHandle = new Color(0.0f, 0.5f, 1.0f, 0.75f);

        public static readonly Color ColorCurveTransition = new Color(1.0f, 0.0f, 0.5f); // new Color(0.309f, 0.501f, 1.0f);

        public static readonly Color ColorCurveGizmo = new Color(1.0f, 1.0f, 1.0f, 0.1f);
        public static readonly Color ColorCurveInstanceGizmo = new Color(1.0f, 1.0f, 1.0f, 0.1f);
        public static readonly Color ColorCurveHandles = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        public static readonly Color ColorCurveInstanceHandles = new Color(0.5f, 0.0f, 0.5f, 1.0f);

        #endregion

        // fields
        #region Fields
        private static int _ccMouseButton = 1;

        private static bool _optionOrientedPointHandles = false;
        private static bool _optionOrientedTangentHandles = false;
        private static bool _optionDrawGizmos = true;
        private static bool _optionShowHelp = false;

        private static int _curveEditorTool = 0;
        private static bool _curveEditorSegmentFoldout = true;
        private static CurveSegmentElements _curveEditorAlignmentTargets = (CurveSegmentElements)int.MaxValue;
        private static CurveSegmentAxes _curveEditorAlignmentAxis = CurveSegmentAxes.X;
        private static CurveSegmentVerbosity _curveEditorSegmentVerbosity = CurveSegmentVerbosity.Full;

        #endregion

        // properties
        #region Properties
        public static bool OptionOientedPointHandles
        {
            get { return _optionOrientedTangentHandles; }
            set { _optionOrientedTangentHandles = value; }
        }
        public static bool OptionOientedTangentHandles
        {
            get { return _optionOrientedPointHandles; }
            set { _optionOrientedPointHandles = value; }
        }
        public static bool OptionDrawGizmos
        {
            get { return _optionDrawGizmos; }
            set { _optionDrawGizmos = value; }
        }
        public static bool OptionShowHelp
        {
            get { return _optionShowHelp; }
            set { _optionShowHelp = value; }
        }
        public static bool OptionCurveSegmentFoldout
        {
            get { return _curveEditorSegmentFoldout; }
            set { _curveEditorSegmentFoldout = value; }
        }
        public static CurveSegmentVerbosity OptionCurveSegmentVerbosity
        {
            get { return _curveEditorSegmentVerbosity; }
            set { _curveEditorSegmentVerbosity = value; }
        }

        public static int CurveEditorTool
        {
            get { return _curveEditorTool; }
            set { _curveEditorTool = value; }
        }
        public static CurveSegmentElements CurveEditorAlignmentTargets
        {
            get { return _curveEditorAlignmentTargets; }
            set { _curveEditorAlignmentTargets = value; }
        }
        public static CurveSegmentAxes CurveEditorAlignmentAxis
        {
            get { return _curveEditorAlignmentAxis; }
            set { _curveEditorAlignmentAxis = value; }
        }

        #endregion


        private static bool BeginCurveEdit(GameObject target)
        {
            if (target != null && _ccParam.Curve == null)
            {
                _ccParam.Curve = target.GetComponent<Curve>();
                // reset the curve's segments to default (one)
                _ccParam.Curve.Clear();
                _ccParam.Segments = 1;
                return true;
            }
            return false;
        }
        private static bool ContinueCurveEdit(GameObject target)
        {
            if (target != null && _ccParam.Curve == null)
            {
                _ccParam.Curve = target.GetComponent<Curve>();
                _ccParam.Init = true;
                _ccParam.Segments = _ccParam.Curve.NumberOfSegments + 1;
                return true;
            }
            return false;

        }
        private static bool EndCurveEdit()
        {
            if (_ccParam.Curve != null)
            {
                _ccParam.Reset();
                return true;
            }
            return false;
        }
        public static void ProcessCurveEdit(Vector3 pivot, Camera camera)
        {
            Event ev = Event.current;
            // ! ! ! ! !
            // disabled until a general approach for hotkeys is developed
            /*
            if (Event.current.keyCode == KeyCode.N
                && Event.current.type == EventType.KeyUp)
            {
                CurveHelper.BeginCurveEdit();
            }

            if (Event.current.keyCode == KeyCode.C
                && Event.current.type == EventType.KeyUp)
            {
                if (!CurveHelper.ContinueCurveEdit())
                    CurveHelper.EndCurveEdit();
            }
            */

            if (_ccParam.Curve != null)
            {
                if (ev.button == _ccMouseButton
                    && ev.modifiers != EventModifiers.Alt
                    && ev.type == EventType.MouseDown)
                {
                    ev.Use();

                    // create at mouse location
                    float distance = (pivot - camera.transform.position).magnitude;

                    _ccParam.Vectors[(int)CCVectors.VP2W00] = (camera.ViewportToWorldPoint(new Vector3(0, 0, distance)));
                    _ccParam.Vectors[(int)CCVectors.VP2W10] = (camera.ViewportToWorldPoint(new Vector3(1, 0, distance)));
                    _ccParam.Vectors[(int)CCVectors.VP2W01] = (camera.ViewportToWorldPoint(new Vector3(0, 1, distance)));

                    _ccParam.Vectors[(int)CCVectors.WRight] = _ccParam.Vectors[(int)CCVectors.VP2W10] - _ccParam.Vectors[(int)CCVectors.VP2W00];
                    _ccParam.Vectors[(int)CCVectors.WUp] = _ccParam.Vectors[(int)CCVectors.VP2W01] - _ccParam.Vectors[(int)CCVectors.VP2W00];

                    // use svCamera pixel size instead of Screen.width/height as those values tend to include menu bars
                    _ccParam.Vectors[(int)CCVectors.VPMouse] = new Vector3(ev.mousePosition.x / camera.pixelWidth, 1.0f - (ev.mousePosition.y / camera.pixelHeight), 0);
                    Vector3 position = _ccParam.Vectors[(int)CCVectors.VP2W00]
                                        + _ccParam.Vectors[(int)CCVectors.VPMouse].x * _ccParam.Vectors[(int)CCVectors.WRight]
                                        + _ccParam.Vectors[(int)CCVectors.VPMouse].y * _ccParam.Vectors[(int)CCVectors.WUp];

                    CurveSegment seg = _ccParam.Curve[_ccParam.Curve.NumberOfSegments - 1];
                    Vector3 cp = _ccParam.Curve.transform.InverseTransformPoint(position);
                    if (!_ccParam.Init)
                    {
                        seg.Start = seg.End = cp;
                        seg.StartTangent = seg.EndTangent = Vector3.forward * 0.01f;
                        _ccParam.Init = true;
                    }
                    else
                    {
                        if (_ccParam.Segments > _ccParam.Curve.NumberOfSegments)
                        {
                            seg = new CurveSegment(seg.End, seg.EndTangent, seg.End, Vector3.forward);
                            seg.EndTangentMode = CurveTangentModes.Shared;
                            _ccParam.Curve.Add(seg);
                        }
                        Vector3 tangent = (cp - seg.Start) * 0.25f;
                        seg.EndTangentMode = CurveTangentModes.Individual;
                        seg.End = cp;
                        seg.StartTangent = tangent;
                        seg.EndTangent = tangent;

                        // increase counter of segments (which is used to check next time a point is set to determine if a new segment needs to be created
                        //  ->  this prevents additional segments to be created ahead of time (and to be removed afterwards
                        _ccParam.Segments++;
                    }

                }

            }

            if (ev.button == _ccMouseButton
                && ev.modifiers != EventModifiers.Alt
                && ev.type == EventType.MouseDrag)
            {
                if (_ccParam.Curve != null)
                {
                    // use svCamera pixel size instead of Screen.width/height as those values tend to include menu bars
                    Vector3 viewport = new Vector3(ev.mousePosition.x / camera.pixelWidth, 1.0f - (ev.mousePosition.y / camera.pixelHeight), 0);
                    // create at mouse location
                    float distance = (pivot - camera.transform.position).magnitude;

                    Vector3 position = _ccParam.Vectors[(int)CCVectors.VP2W00]
                                        + viewport.x * _ccParam.Vectors[(int)CCVectors.WRight]
                                        + viewport.y * _ccParam.Vectors[(int)CCVectors.WUp];

                    CurveSegment seg = _ccParam.Curve[_ccParam.Curve.NumberOfSegments - 1];

                    float viewportDistance = (viewport - _ccParam.Vectors[(int)CCVectors.VPMouse]).magnitude;
                    if (viewportDistance > 0.01f)
                    {
                        if (_ccParam.Curve.NumberOfSegments >= 2)
                            _ccParam.Curve[_ccParam.Curve.NumberOfSegments - 2].EndTangentMode = CurveTangentModes.Shared;
                        seg.EndTangent = _ccParam.Curve.transform.InverseTransformDirection(_ccParam.Curve.transform.TransformPoint(seg.End) - position);
                    }
                    else
                    {
                        if (_ccParam.Curve.NumberOfSegments > 1)
                            _ccParam.Curve[_ccParam.Curve.NumberOfSegments - 2].EndTangentMode = CurveTangentModes.Individual;
                        seg.EndTangent = (seg.End - seg.Start) * 0.25f;
                        _ccParam.Curve[_ccParam.Curve.NumberOfSegments - 1].StartTangent = seg.StartTangent;
                        seg.StartTangent = seg.EndTangent;
                    }

#if UNITY_EDITOR
                    // only draw handles when used in editor context
                    Handles.Label(position, viewportDistance.ToString("0.000"));
#endif
                }
            }
        }


        #region Logical Helper
        public static void RefreshCurveTransition(CurveTransition transition, bool self, bool source, bool target)
        {
            if (transition != null)
            {
                if (transition.Curve != null && self)
                {
                    transition.Curve.InitExtensions();
                    transition.Curve.RefreshExtensions();
                }

                if (transition.Source != null && source)
                    CurveHelper.RefreshCurve(transition.Source);
                if (transition.Target != null && target)
                    CurveHelper.RefreshCurve(transition.Target);

                if (self)
                    transition.Refresh();

            }
        }
        public static void RefreshCurve(CurveBase curveBase)
        {
            if (curveBase is Curve)
            {
                Curve curve = (Curve)curveBase;
                curve.RefreshConciseData();
                curve.InitExtensions();
                curve.RefreshExtensions();
            }
            else if (curveBase is CurveInstance)
            {
                CurveInstance curveInstance = (CurveInstance)curveBase;
                RefreshCurve(curveInstance.Curve);
            }
        }

        // mostly an editor-only method (which might be used in runtime but this is far from done
        public static void CheckCurveEditorShortcuts()
        {
            Event ev = Event.current;
#if UNITY_EDITOR
            // preveint deselection of curve when left clicking
            //if (ev.type == EventType.Layout)
            //    HandleUtility.AddDefaultControl(0);
#endif
            if (ev.type == EventType.KeyUp)
            {
                if (ev.modifiers == EventModifiers.Shift)
                {
                    if (ev.keyCode == KeyCode.Alpha1)
                        CurveHelper.CurveEditorTool = 0;
                    if (ev.keyCode == KeyCode.Alpha2)
                        CurveHelper.CurveEditorTool = 1;
                    if (ev.keyCode == KeyCode.Alpha3)
                        CurveHelper.CurveEditorTool = 2;
                    if (ev.keyCode == KeyCode.Alpha4)
                        CurveHelper.CurveEditorTool = 3;
                    if (ev.keyCode == KeyCode.Alpha5)
                        CurveHelper.CurveEditorTool = 4;
                    if (ev.keyCode == KeyCode.Alpha6)
                        CurveHelper.CurveEditorTool = 5;
                }
                else
                {
                    // reset current tool to Modify when any of the Move, Rotate, Scale tool is selected
                    // checking for key inputs does break the unity internal stuff and activation of the default tools needs to be done manually
                    if (CurveHelper.CurveEditorTool == 5
                        && (ev.keyCode == KeyCode.Q
                        || ev.keyCode == KeyCode.W
                        || ev.keyCode == KeyCode.E
                        || ev.keyCode == KeyCode.R))
                    {
                        CurveHelper.CurveEditorTool = 0;
#if UNITY_EDITOR
                        if (ev.keyCode == KeyCode.Q) UnityEditor.Tools.current = Tool.View;
                        else if (ev.keyCode == KeyCode.W) UnityEditor.Tools.current = Tool.Move;
                        else if (ev.keyCode == KeyCode.E) UnityEditor.Tools.current = Tool.Rotate;
                        else if (ev.keyCode == KeyCode.R) UnityEditor.Tools.current = Tool.Scale;
#endif
                    }
                }
            }
        }

        #endregion

        #region Value Helper
        public static Vector3 Round(Vector3 value, int decimals)
        {
            return new Vector3((float)Math.Round(value.x, decimals), (float)Math.Round(value.y, decimals), (float)Math.Round(value.z, decimals));
        }

        public static Vector3 Zero(CurveSegmentAxes axis, Vector3 value)
        {
            if (axis == CurveSegmentAxes.X)
                value.x = 0.0f;
            else if (axis == CurveSegmentAxes.Y)
                value.y = 0.0f;
            else if (axis == CurveSegmentAxes.Z)
                value.z = 0.0f;
            return value;
        }
        #endregion

        #region Format Helper (unfinished, in-progress)

        public void CurveFromAnimation(AnimationClip clip)
        {

        }

        public CurveSegment[] SegemntsFromSVG(string svgpath) // svgpath means the content of the 'd'-attribute of an svg path
        {
            // d = "M0.5,34.879c147.368,125.685 257.895,-120.842 377.684,0"
            // initial point is the value after M (+ if present the base matrix transformation)
            //  first coord-pair = first controlpoint (absolute start tangent position)
            //  second coord-pair = first controlpoint (absolute start tangent position)
            //  third coord-pait = end point of curve (and also start of next segment)
            // ==============
            // general processing
            //  get start position from M/m block
            //      get number of C/c blocks to get number of segments created
            //          generate segment points/tangents from each C/c block
            return null;
        }

        #endregion


#if UNITY_EDITOR

        #region IMGUIWindow - CurveTool-Window

        private static IMGUIWindow _imguiWinCurveTool = new IMGUIWindow(3237, "Curve Tool", new Rect(5, 24, 200, 95), CurveHelper.CurveToolWindow, true);
        public static IMGUIWindow WindowCurveTool
        { get { return _imguiWinCurveTool; } }
        private static void CurveToolWindow(Rect contentRect)
        {
            CurveHelper.CurveEditorTool = GUILayout.Toolbar(CurveHelper.CurveEditorTool,
                                                                new GUIContent[] {CurveResources.GUICONT_CurveToolCursor, CurveResources.GUICONT_CurveToolAdd, CurveResources.GUICONT_CurveToolRemove,
                                                                                    CurveResources.GUICONT_CurveToolCorner, CurveResources.GUICONT_CurveToolSmooth, CurveResources.GUICONT_CurveToolPivot},
                                                                EditorStyles.toolbarButton);

            using (new EditorGUILayout.HorizontalScope())
            {
                using (new EditorGUI.DisabledGroupScope(_ccParam.Curve != null))
                {
                    if (GUILayout.Button(CurveResources.GUICONT_CurveToolPencilAdd, EditorStyles.toolbarButton))
                        CurveHelper.ContinueCurveEdit(Selection.activeGameObject);
                    if (GUILayout.Button(CurveResources.GUICONT_CurveToolPencil, EditorStyles.toolbarButton))
                        CurveHelper.BeginCurveEdit(Selection.activeGameObject);
                }
                using (new EditorGUI.DisabledGroupScope(_ccParam.Curve == null))
                {
                    if (GUILayout.Button(CurveResources.GUICONT_CurveToolPencilCancel, EditorStyles.toolbarButton))
                        CurveHelper.EndCurveEdit();
                }
                GUILayout.Toggle(false, CurveResources.UIRES_CurveToolOptionEmpty, EditorStyles.toolbarButton);
                GUILayout.Toggle(false, CurveResources.UIRES_CurveToolOptionEmpty, EditorStyles.toolbarButton);
                GUILayout.Toggle(false, CurveResources.UIRES_CurveToolOptionEmpty, EditorStyles.toolbarButton);

            }

            GUILayout.Space(-2.0f);
            EditorGUILayout.LabelField("Options", EditorStyles.miniLabel);
            using (new EditorGUILayout.HorizontalScope())
            {
                CurveHelper.OptionOientedPointHandles = GUILayout.Toggle(CurveHelper.OptionOientedPointHandles, CurveResources.GUICONT_CurveToolOptionPoints, EditorStyles.toolbarButton);
                CurveHelper.OptionOientedTangentHandles = GUILayout.Toggle(CurveHelper.OptionOientedTangentHandles, CurveResources.GUICONT_CurveToolOptionTangents, EditorStyles.toolbarButton);
                CurveHelper.OptionDrawGizmos = GUILayout.Toggle(CurveHelper.OptionDrawGizmos, CurveResources.GUICONT_CurveToolOptionGizmo, EditorStyles.toolbarButton);
                EditorGUI.BeginChangeCheck();
                CurveHelper.OptionShowHelp = GUILayout.Toggle(CurveHelper.OptionShowHelp, CurveResources.GUICONT_CurveToolOptionHelp, EditorStyles.toolbarButton);
                if (EditorGUI.EndChangeCheck())
                    _imguiWinCurveTool.Height = CurveHelper.OptionShowHelp ? 180 : 95;
                GUILayout.Toggle(false, CurveResources.UIRES_CurveToolOptionEmpty, EditorStyles.toolbarButton);
                GUILayout.Toggle(false, CurveResources.UIRES_CurveToolOptionEmpty, EditorStyles.toolbarButton);
            }

            //GUILayout.Space(-2.0f);
            //EditorGUILayout.Separator();
            using (new EditorGUILayout.VerticalScope())
            {
                EditorGUILayout.HelpBox("Hold down [left shift] when using a tool.", MessageType.Info, true);
                EditorGUILayout.HelpBox("The tool uses [RMB] for interaction in scene.", MessageType.Info, true);
            }

            // button to fill the complete window to prevent click-through deselect
            GUI.Button(contentRect, "", EditorStyles.label);
        }

        #endregion

        #region Drawing Helper - SceneGUI
        public static void SceneViewRepaintAll()
        {
            SceneView.RepaintAll();
        }

        public static void DrawSceneGUI()
        {
            Handles.BeginGUI();

            _imguiWinCurveTool.Draw();
            // check for shortcut inputs
            CurveHelper.CheckCurveEditorShortcuts();
            Handles.EndGUI();
        }

        // ! ! ! !
        // change the bool "drawHandles" into a single float "handleSize" which disables handles when <= 0.0f
        public static bool SceneDrawCurve(Curve curve, bool drawHandles, Color curveColor, float handleSize = 1.0f, Transform transform = null)
        {
            if (curve == null)
                return false;
            // check if no transform is given and use the curves transform as default!
            if (transform == null)
                transform = curve.transform;

            EditorGUI.BeginChangeCheck();
            {
                if (curve != null)
                {
                    for (int i = 0; i < curve.NumberOfSegments; i++)
                        CurveHelper.SceneDrawSegment(curve, i, curveColor, transform);

                    if (drawHandles)
                    {
                        // draw point handles
                        for (int i = 0; i < curve.NumberOfSegments; i++)
                            CurveHelper.SceneDrawPointHandle(curve, i, OptionOientedPointHandles, handleSize, transform);

                        // draw tangent handles
                        for (int i = 0; i < curve.NumberOfSegments; i++)
                            CurveHelper.SceneDrawTangentHandle(curve, i, OptionOientedTangentHandles, handleSize, transform);
                    }
                }
            }
            return EditorGUI.EndChangeCheck();
        }
        protected static void SceneDrawPointHandle(Curve curve, int segIndex, bool orientedHandles, float handleScale = 1.0f, Transform transform = null)
        {
            if (segIndex >= curve.NumberOfSegments)
                return;

            CurveSegment seg = curve[segIndex];


            Quaternion rotation = Quaternion.identity;
            if (UnityEditor.Tools.pivotRotation == PivotRotation.Local)
                rotation = transform.rotation;

            Vector3 tfsStartPos = transform.TransformPoint(seg.Start);
            Vector3 tfsEndPos = transform.TransformPoint(seg.End);

            // get the relative handle size for the curve's position
            float hSizeStart = HandleUtility.GetHandleSize(tfsStartPos) * handleScale;
            float hSizeEnd = HandleUtility.GetHandleSize(tfsEndPos) * handleScale;
            // draw the position handle (square)

            if (segIndex == 0)
            {
                seg.Start = (CurveHelper.Round(transform.InverseTransformPoint(
                (!orientedHandles ? Handles.FreeMoveHandle(tfsStartPos, rotation, hSizeStart / 8, Vector3.zero, Handles.RectangleCap)
                                   : Handles.PositionHandle(tfsStartPos, rotation))), 3));
                // draw the start-dot of the curve
                Handles.color = Color.green;
                Handles.SphereCap(0, tfsStartPos, Quaternion.identity, hSizeStart / 8);
                Handles.color = CurveHelper.ColorCurveHandle;
            }
            seg.End = (CurveHelper.Round(transform.InverseTransformPoint(
                (!orientedHandles ? Handles.FreeMoveHandle(tfsEndPos, rotation, hSizeEnd / 8, Vector3.zero, Handles.RectangleCap)
                                   : Handles.PositionHandle(tfsEndPos, rotation))), 3));

            // set the next segments start point to the current segment end point
            if (segIndex > 0)
                curve[segIndex - 1].End = seg.Start;

            // set the next segments start point to the current segment end point
            if (segIndex < curve.NumberOfSegments - 1) // check if there is at least on segment after the current
                curve[segIndex + 1].Start = seg.End;
        }
        protected static void SceneDrawTangentHandle(Curve curve, int segIndex, bool orientedHandles, float handleScale = 1.0f, Transform transform = null)
        {
            // check if curve is NOT linear (tangent handles are not required for linear curves)
            if (curve.Type == CurveTypes.Linear)
                return;
            if (segIndex >= curve.NumberOfSegments)
                return;

            CurveSegment seg = curve[segIndex];

            Quaternion rotation = Quaternion.identity;
            if (UnityEditor.Tools.pivotRotation == PivotRotation.Local)
                rotation = transform.rotation;

            Vector3 tfsStartPos = transform.TransformPoint(seg.Start);
            Vector3 tfsEndPos = transform.TransformPoint(seg.End);

            // calc tranformed tangent and reversed tangent
            Vector3 tfsStartTan = transform.TransformPoint(seg.Start + seg.StartTangent);
            Vector3 tfsEndTan = transform.TransformPoint(seg.End - seg.EndTangent);

            // get the relative handle size for the BezierPoint position
            float hSizeStart = HandleUtility.GetHandleSize(tfsStartTan) * handleScale;
            float hSizeEnd = HandleUtility.GetHandleSize(tfsEndTan) * handleScale;
            // draw line from position to tangent handle    
            Handles.color = CurveHelper.ColorTangent;
            Handles.DrawLine(tfsStartPos, tfsStartTan);
            Handles.DrawLine(tfsEndPos, tfsEndTan);
            // draw the tangent handle (circle)
            Handles.color = CurveHelper.ColorTangentHandle;

            Vector3 newStartTangent = CurveHelper.Round((transform.InverseTransformPoint(
                !orientedHandles ? Handles.FreeMoveHandle(tfsStartTan, rotation, hSizeStart / 6, Vector3.zero, Handles.SphereCap)
                                    : Handles.PositionHandle(tfsStartTan, rotation)) - seg.Start), 3);

            Vector3 newEndTangent = CurveHelper.Round(-(transform.InverseTransformPoint(
                !orientedHandles ? Handles.FreeMoveHandle(tfsEndTan, rotation, hSizeEnd / 6, Vector3.zero, Handles.SphereCap)
                                    : Handles.PositionHandle(tfsEndTan, rotation)) - seg.End), 3);

            if (curve[segIndex].StartTangent != newStartTangent)
                curve[segIndex].StartTangent = newStartTangent;

            if (curve[segIndex].EndTangent != newEndTangent)
                curve[segIndex].EndTangent = newEndTangent;

            // add additional tangent handles for first and last point
            #region Additional start/end tangent handles for first and last segment
            if (segIndex == 0)
            {
                Vector3 tfsStartTanInv = transform.TransformPoint(seg.Start - seg.StartTangent);
                Handles.color = CurveHelper.ColorTangent;
                Handles.DrawLine(tfsStartPos, tfsStartTanInv);
                Handles.color = CurveHelper.ColorTangentHandle;
                newStartTangent = -CurveHelper.Round((transform.InverseTransformPoint(
                    !orientedHandles ? Handles.FreeMoveHandle(tfsStartTanInv, rotation, hSizeStart / 6, Vector3.zero, Handles.SphereCap)
                                        : Handles.PositionHandle(tfsStartTanInv, rotation)) - seg.Start), 3);
                if (curve[segIndex].StartTangent != newStartTangent)
                    curve[segIndex].StartTangent = newStartTangent;
            }
            if (segIndex == curve.NumberOfSegments - 1)
            {
                Vector3 tfsEndTanInv = transform.TransformPoint(seg.End + seg.EndTangent);
                Handles.color = CurveHelper.ColorTangent;
                Handles.DrawLine(tfsEndPos, tfsEndTanInv);
                Handles.color = CurveHelper.ColorTangentHandle;
                newEndTangent = -CurveHelper.Round(-(transform.InverseTransformPoint(
                    !orientedHandles ? Handles.FreeMoveHandle(tfsEndTanInv, rotation, hSizeStart / 6, Vector3.zero, Handles.SphereCap)
                                        : Handles.PositionHandle(tfsEndTanInv, rotation)) - seg.End), 3);
                if (curve[segIndex].EndTangent != newEndTangent)
                    curve[segIndex].EndTangent = newEndTangent;
            }
            #endregion

            // set the next segments start point to the current segment end point
            if (segIndex > 0)
            {
                if (curve[segIndex - 1].EndTangentMode == CurveTangentModes.Shared)
                    curve[segIndex - 1].EndTangent = curve[segIndex].StartTangent;
            }
            // set the next segments start point to the current segment end point
            if (segIndex < curve.NumberOfSegments - 1) // check if there is at least on segment after the current
            {
                if (curve[segIndex].EndTangentMode == CurveTangentModes.Shared)
                    curve[segIndex + 1].StartTangent = curve[segIndex].EndTangent;
            }
        }
        protected static void SceneDrawSegment(Curve curve, int segIndex, Color curveColor, Transform transform = null)
        {
            CurveSegment seg = curve[segIndex];

            Vector3 tfsStartPos = transform.TransformPoint(seg.Start);
            Vector3 tfsEndPos = transform.TransformPoint(seg.End);
            if (seg.IsLink)
            {
                // draw link segments as dotted lines (to display them but distinguish between link and all other segments
                if (curve.Type == CurveTypes.Linear) Handles.DrawDottedLine(tfsStartPos, tfsEndPos, 2);
                else CurveHelper.HandlesDrawDottedSegment(curve, seg, 8, 2, transform);
            }
            else if (curve.Type == CurveTypes.Bezier)
            {
                Vector3 tfsStartTan = transform.TransformPoint(seg.Start + seg.StartTangent);
                Vector3 tfsEndTan = transform.TransformPoint(seg.End - seg.EndTangent);
                // manipulating the end point (locking the tangent to the offset)
                Handles.DrawBezier(tfsStartPos, tfsEndPos, tfsStartTan, tfsEndTan, curveColor, null, 5);
            }
            else if (curve.Type == CurveTypes.Linear)
            {
                Handles.color = curveColor;
                Handles.DrawAAPolyLine(5, tfsStartPos, tfsEndPos);
            }
        }
        #endregion
        #region Drawing Helper - InspectorGUI
        public static void InspectorRepaint(Editor editor)
        {
            if (editor != null)
                editor.Repaint();
        }
        public static void InspectorDrawWindowToggles()
        {
            EditorGUI.BeginChangeCheck();
            {
                using (new GUILayout.HorizontalScope())
                {
                    CurveHelper.WindowCurveTool.IsVisible = GUILayout.Toggle(CurveHelper.WindowCurveTool.IsVisible, "Curve Tools", EditorStyles.miniButton);
                }
            }
            if (EditorGUI.EndChangeCheck())
                CurveHelper.SceneViewRepaintAll();
        }

        public static void InspectorDrawUtilities(CurveBase[] curves)
        {
            float width = (Screen.width - 24) / 3;
            GUILayoutOption[] guiOptions = new GUILayoutOption[] { GUILayout.MinWidth(width), GUILayout.MaxWidth(width) };

            //bool hasCurves = curves.Count(x => x is Curve) > 0;
            bool hasCurveInstances = curves.Count(x => x is CurveInstance) > 0;


            EditorGUILayout.LabelField("Utilities", EditorStyles.miniBoldLabel);
            using (new EditorGUILayout.HorizontalScope())
            {
                CurveHelper.InspectorDrawUtil_Instantiate(guiOptions, curves);
                using (new EditorGUI.DisabledGroupScope(!hasCurveInstances))
                    CurveHelper.InspectorDrawUtil_Deinstantiate(guiOptions, curves);
                CurveHelper.InspectorDrawUtil_Reverse(guiOptions, curves);
            }
            GUILayout.Space(2.0f);

            using (new EditorGUILayout.HorizontalScope())
            {
                CurveHelper.InspectorDrawUtil_Close(guiOptions, curves);
                CurveHelper.InspectorDrawUtil_Transition(guiOptions, width, curves);
            }
            GUILayout.Space(4.0f);

            using (new EditorGUILayout.HorizontalScope())
                CurveHelper.InspectorDrawUtil_Align(guiOptions, curves);

            GUILayout.Space(4.0f);
        }

        private static void InspectorDrawUtil_Align(GUILayoutOption[] guiOptions, params CurveBase[] curves)
        {
            if (GUILayout.Button("Align", EditorStyles.miniButton, guiOptions))
            {
                foreach (Curve curve in curves)
                {
                    // change to a custom event handler
                    Undo.RecordObject(curve, "Align Curve(s) elements");
                    curve.AlignSegmentsToAxis(CurveHelper.CurveEditorAlignmentAxis, CurveHelper.CurveEditorAlignmentTargets);
                }
            }
            //CurveHelper.CurveEditorAlignmentAxis = (CurveSegmentAxes)GUILayout.Toolbar((int)CurveHelper.CurveEditorAlignmentAxis, new string[] { "X-Axis", "Y-Axis", "Z-Axis" }, EditorStyles.toolbarButton);
            CurveHelper.CurveEditorAlignmentTargets = (CurveSegmentElements)EditorGUILayout.EnumMaskField(CurveHelper.CurveEditorAlignmentTargets, EditorStyles.popup, guiOptions);
            CurveHelper.CurveEditorAlignmentAxis = (CurveSegmentAxes)EditorGUILayout.EnumPopup(CurveHelper.CurveEditorAlignmentAxis, EditorStyles.popup, guiOptions);

        }
        private static void InspectorDrawUtil_Instantiate(GUILayoutOption[] guiOptions, params CurveBase[] curves)
        {
            if (GUILayout.Button("Instantiate", EditorStyles.miniButton, guiOptions))
            {
                foreach (CurveBase curveBase in curves)
                {
                    GameObject goInst = CurveInstance.Instantiate(curveBase);
                    if (goInst != null)
                    {
                        Selection.activeGameObject = goInst;
                        Undo.RegisterCreatedObjectUndo(goInst, "Create Curve Instance(s)");
                    }
                }
            }
        }
        private static void InspectorDrawUtil_Close(GUILayoutOption[] guiOptions, params CurveBase[] curves)
        {
            if (GUILayout.Button("Close", EditorStyles.miniButton, guiOptions))
            {
                foreach (Curve curve in curves)
                {
                    Undo.RecordObject(curve, "Close Curve(s)");
                    curve.CloseEndSegmentToStart();
                }
            }
        }
        private static void InspectorDrawUtil_Reverse(GUILayoutOption[] guiOptions, params CurveBase[] curves)
        {
            if (GUILayout.Button("Reverse", EditorStyles.miniButton, guiOptions))
            {
                foreach (Curve curve in curves)
                {
                    Undo.RecordObject(curve, "Reverse Curve(s)");
                    curve.ReverseCompleteCurve();
                }
            }
        }

        private static CurveBase _cb0, _cb1;
        private static void InspectorDrawUtil_Transition(GUILayoutOption[] guiOptions, float width, params CurveBase[] curves)
        {
            if (curves.Length >= 1)
            {
                if (GUILayout.Button("Transition", EditorStyles.miniButton, guiOptions))
                {
                    if (curves.Length > 1)
                        CurveTransition.CreateCurveTransition(curves[0], curves[1]);
                    else if (_cb0 != null && _cb1 != null)
                        CurveTransition.CreateCurveTransition(_cb0, _cb1);
                }

                _cb0 = (CurveBase)EditorGUILayout.ObjectField(_cb0, typeof(CurveBase), true, GUILayout.MaxWidth(width / 2));
                _cb1 = (CurveBase)EditorGUILayout.ObjectField(_cb1, typeof(CurveBase), true, GUILayout.MaxWidth(width / 2));
            }
        }
        private static void InspectorDrawUtil_Deinstantiate(GUILayoutOption[] guiOptions, params CurveBase[] curves)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Deinstantiate", EditorStyles.miniButton, guiOptions))
                {
                    foreach (CurveInstance curveInst in curves)
                    {
                        Undo.RegisterFullObjectHierarchyUndo(curveInst.gameObject, "Deinstantiate Instance(s)");
                        CurveInstance.Deinstantiate(curveInst);
                    }
                }

            }
        }

        #endregion
        #region Drawing Helper - Gizmos
        public static void GizmosDrawCurve(Curve curve, int stepsPerSegment, Transform transform = null)
        {
            if (transform == null)
                transform = curve.transform;

            Color gizClr = Gizmos.color;
            float segTimeStep = 1.0f / stepsPerSegment;
            for (int i = 0; i < curve.NumberOfSegments; i++)
            {
                // set gizmo color to transparent if the segment is a link segment
                Gizmos.color = curve[i].IsLink ? CurveHelper.ColorTransparent : gizClr;
                for (int j = 0; j < stepsPerSegment; j++)
                    Gizmos.DrawLine(transform.TransformPoint(Curve.Sample(curve[i], segTimeStep * j, curve.Type)),
                                    transform.TransformPoint(Curve.Sample(curve[i], segTimeStep * (j + 1), curve.Type)));
            }
            Gizmos.color = gizClr;
        }

        #endregion
        #region Drawing Helper - Handles
        public static void HandlesDrawDottedSegment(Curve curve, CurveSegment segment, int steps, float screenSpaceSize, Transform transform = null)
        {
            if (transform == null)
                transform = curve.transform;

            float stepSpan = 1.0f / steps;
            Vector3[] points = new Vector3[steps + 1];
            points[0] = transform.TransformPoint(Curve.Sample(segment, 0.0f, curve.Type));
            for (int i = 1; i <= steps; i++)
                points[i] = transform.TransformPoint(Curve.Sample(segment, i * stepSpan, curve.Type));

            CurveHelper.HandlesDrawDottedLines(screenSpaceSize, points);
        }
        public static void HandlesDrawDottedLines(float screenSpaceSize, params Vector3[] points)
        {
            if (points.Length > 1)
                for (int i = 1; i < points.Length; i++)
                    Handles.DrawDottedLine(points[i - 1], points[i], 2);
        }

        #endregion

        #region Unity Menu Entries

        [MenuItem("GameObject/3D Object/Curve &%c")]
        public static void MenuItem_CreateCurve()
        {
            if (Selection.activeGameObject == null)
                Selection.activeGameObject = new GameObject("Curve");
            if (Selection.activeGameObject != null && _ccParam.Curve == null)
            {
                _ccParam.Curve = Undo.AddComponent<Curve>(Selection.activeGameObject);
                _ccParam.Curve[0].End = new Vector3(0, 0, 20);
            }
        }

        //[MenuItem("GameObject/3D Object/Curve &%b")]
        public static void SceneClean_HiddenCurveObjects()
        {
            Curve[] curves = GameObject.FindObjectsOfType<Curve>();
            foreach (Curve c in curves)
                if (c.hideFlags == HideFlags.HideInHierarchy)
                    GameObject.DestroyImmediate(c.gameObject);
        }
        #endregion

#endif

    }


    public class EnumMaskAttribute : PropertyAttribute
    {
        public EnumMaskAttribute() { }
    }

#if UNITY_EDITOR

    #region EditorGUI - PropertyDrawer
    [CustomPropertyDrawer(typeof(EnumMaskAttribute))]
    public class EnumMaskAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Undo.RecordObject(property.serializedObject.targetObject, "Modified Enum Mask");
            property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
        }
    }

    [CustomPropertyDrawer(typeof(CurveTransformOptions))]
    public class CurveTransformOptionsDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 36.0f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty spLoop = property.FindPropertyRelative("Loop");
            SerializedProperty spReverse = property.FindPropertyRelative("Reverse");
            SerializedProperty spOrientToCurve = property.FindPropertyRelative("OrientToCurve");
            SerializedProperty spFollowTransition = property.FindPropertyRelative("FollowTransition");

            Rect ctrlRect = EditorGUI.IndentedRect(position);
            float indentWidth = (ctrlRect.x - position.x);
            ctrlRect.height = 16.0f;
            GUI.Label(ctrlRect, property.displayName, EditorStyles.boldLabel);
            //ctrlRect.y += 16.0f;
            ctrlRect.width = (ctrlRect.width - EditorGUIUtility.labelWidth + indentWidth) / 2;
            ctrlRect.x += EditorGUIUtility.labelWidth - indentWidth;


            // second line transform
            // second line 
            spLoop.boolValue = GUI.Toggle(ctrlRect, spLoop.boolValue, spLoop.displayName, EditorStyles.miniButtonLeft);
            ctrlRect.x += ctrlRect.width;
            spReverse.boolValue = GUI.Toggle(ctrlRect, spReverse.boolValue, spReverse.displayName, EditorStyles.miniButtonRight);
            ctrlRect.x -= ctrlRect.width;

            // third line transform
            ctrlRect.y += 16.0f;
            // third line
            spOrientToCurve.boolValue = GUI.Toggle(ctrlRect, spOrientToCurve.boolValue, "Orient", EditorStyles.miniButtonLeft);
            ctrlRect.x += ctrlRect.width;
            spFollowTransition.boolValue = GUI.Toggle(ctrlRect, spFollowTransition.boolValue, "Transition", EditorStyles.miniButtonRight);
            ctrlRect.x -= ctrlRect.width;

        }
    }

    #endregion

#endif



}