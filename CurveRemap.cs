﻿using UnityEngine;
using System.Collections;

namespace NoXP
{

#if UNITY_EDITOR
    [AddComponentMenu(CurveResources.ComponentMenuBase + "Curve Remap")]
#endif
    public class CurveRemap : CurveExtension
    {

        private static readonly AnimationCurve DefaultCurve = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField()]
        private AnimationCurve _remapCurve = AnimationCurve.Linear(0, 0, 1, 1);
        [Range(0, 1)]
        [SerializeField()]
        private float _blend = 1.0f;

        public AnimationCurve RemapCurve
        { get { return _remapCurve; } }


        protected override void Awake()
        {
            base.Awake();
            this.Curve.RegisterExtension(this);
        }

        void Start()
        {
            if (_remapCurve == null)
                _remapCurve = CurveRemap.DefaultCurve;
        }

        public float Evaluate(float time)
        {
            if (_remapCurve != null
                && _blend > 0.0f)
            {
                if (_blend < 1.0f)
                    return Mathf.Lerp(time, _remapCurve.Evaluate(time), _blend);
                else
                    return _remapCurve.Evaluate(time);
            }
            else
                return time;
        }

    }
}