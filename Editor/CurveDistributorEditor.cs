﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace NoXP
{
    [CustomEditor(typeof(CurveDistributor))]
    public class CurveDistributorEditor : Editor
    {
        public enum DistributorMethod
        {
            None,
            Distribute,
            Redistribute,
            Clear
        }


        public static void DrawFrenetFrame(Vector3 position, Vector3 right, Vector3 up, Vector3 forward, float length = 1.0f, float duration = 0.0f)
        {

            if (right != Vector3.zero)
                Debug.DrawRay(position, right * length, Color.red, duration);
            if (up != Vector3.zero)
                Debug.DrawRay(position, up * length, Color.green, duration);
            if (forward != Vector3.zero)
                Debug.DrawRay(position, forward * length, Color.blue, duration);
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DistributorMethod method = DistributorMethod.None;
            if (GUILayout.Button("Distribute"))
                method = DistributorMethod.Distribute;
            if (GUILayout.Button("Redistribute"))
                method = DistributorMethod.Redistribute;
            if (GUILayout.Button("Clear"))
                method = DistributorMethod.Clear;
            if (GUILayout.Button("Orientation"))
            {
                foreach (CurveDistributor cDistr in serializedObject.targetObjects)
                {
                    CurveBase curve = cDistr.GetComponent<CurveBase>();
                    CurveHelper.RefreshCurve(curve);

                    Vector3 P0 = curve.RawLocalPosition(0.0f);
                    Vector3 T0 = curve.transform.TransformDirection((curve.RawLocalPosition(0.0001f) - P0).normalized);
                    Vector3 N0 = Vector3.Cross(cDistr.transform.right, T0).normalized;
                    Vector3 B0 = Vector3.Cross(T0, N0);

                    Vector3 PW0 = curve.transform.TransformPoint(P0);
                    DrawFrenetFrame(PW0, N0, B0, T0, 2, 10);

                    Vector3 B = B0;
                    Vector3 P, T, N;

                    int divisions = 100;
                    float tStep = 1.0f / (divisions - 1);
                    for (int i = 1; i <= divisions; i++)
                    {
                        float t = i * tStep;
                        P = curve.RawLocalPosition(t);
                        T = curve.transform.TransformVector((curve.RawLocalPosition(t + 0.0001f) - P).normalized);
                        N = Vector3.Cross(B, T).normalized;
                        B = Vector3.Cross(N, T);

                        Vector3 PW = curve.transform.TransformPoint(P);
                        DrawFrenetFrame(PW, N, B, T, 2, 10);

                    }

                }

            }

            if (method != DistributorMethod.None)
            {
                foreach (CurveDistributor cDistr in serializedObject.targetObjects)
                {
                    if (method == DistributorMethod.Clear)
                        CurveDistributorEditor.Clear(cDistr);
                    if (method == DistributorMethod.Distribute && cDistr.Prefabs.Length == 0)
                        continue;
                    if (method == DistributorMethod.Redistribute && cDistr.Distributed.Count == 0)
                        continue;

                    // get curve on the distributor component
                    CurveBase curve = cDistr.GetComponent<CurveBase>();
                    CurveHelper.RefreshCurve(curve);
                    int count = 1;
                    float spacing = 1.0f;
                    // clear all distributed prior to new distribution
                    if (method == DistributorMethod.Distribute)
                    {
                        CurveDistributorEditor.Clear(cDistr);

                        if (cDistr.Mode == CurveDistributor.DistributionModes.Time)
                            count = Mathf.CeilToInt(1.0f / cDistr.Spacing);
                        else
                            count = Mathf.CeilToInt(curve.Length / cDistr.Spacing);
                    }
                    else if (method == DistributorMethod.Redistribute)
                    {
                        count = cDistr.Distributed.Count - 1;
                    }
                    // calculate actual spacing between distributed objects
                    spacing = 1.0f / count;


                    if (method == DistributorMethod.Distribute)
                        for (int i = 0; i <= count; i++)
                        {
                            GameObject goInst = GameObject.Instantiate<GameObject>(cDistr.Prefabs[0]);
                            goInst.transform.position = curve.Position(i * spacing);
                            goInst.transform.parent = cDistr.transform;
                            cDistr.Distributed.Add(goInst);
                            Undo.RegisterCreatedObjectUndo(goInst, "Curve Distributor");
                        }
                    else if (method == DistributorMethod.Redistribute)
                        for (int i = 0; i <= count; i++)
                        {
                            cDistr.Distributed[i].transform.position = curve.Position(i * spacing);
                            Undo.RecordObject(cDistr.Distributed[i], "Curve Distributor");
                        }

                }
            }
            base.DrawDefaultInspector();
        }

        public static void Clear(CurveDistributor cDistr)
        {
            cDistr.Distributed.Clear();
            for (int i = cDistr.transform.childCount - 1; i >= 0; i--)
                GameObject.DestroyImmediate(cDistr.transform.GetChild(i).gameObject);
        }

    }

}