﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace NoXP
{
    [CustomEditor(typeof(CurveInstance))]
    public class CurveInstanceEditor : SceneViewEditor
    {

        private CurveInstance curveInst = null;

        private SerializedProperty propCurve = null;

        void OnEnable()
        {
            curveInst = ((CurveInstance)serializedObject.targetObject);
            propCurve = serializedObject.FindProperty("_curve");
            Undo.undoRedoPerformed += new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void Undo_UndoRedoPreformed()
        {
            if (curveInst != null)
            {
                foreach (CurveTransitionInfo transition in curveInst.Transitions)
                    if (transition.Curve != null)
                        CurveHelper.RefreshCurveTransition(transition.Curve.GetComponent<CurveTransition>(), true, false, false);
                if (curveInst.Curve is Curve)
                    ((Curve)curveInst.Curve).RefreshExtensions();
            }
            CurveHelper.SceneViewRepaintAll();
            CurveHelper.InspectorRepaint(this);
        }

        protected override void HandleChanges()
        {
            foreach (CurveTransitionInfo transition in curveInst.Transitions)
            {
                if (transition.Curve != null)
                {
                    CurveHelper.RefreshCurveTransition(transition.Curve.GetComponent<CurveTransition>(), true, false, false);
                    EditorUtility.SetDirty(transition.Curve);
                }
            }

            if (curveInst.Curve != null)
            {
                Undo.RegisterCompleteObjectUndo(curveInst.Curve, "Modify Curve");
                EditorUtility.SetDirty(curveInst.Curve);
            }
            Undo.RegisterCompleteObjectUndo(curveInst, "Modify Curve");
            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(curveInst);
            CurveHelper.SceneViewRepaintAll();

        }

        protected override void HandleChangesInSceneView()
        {
            //throw new NotImplementedException();
        }


        public override void OnInspectorGUI()
        {
            // handle changes made in the scene view (mostly update stuff not accessible in OnSceneGUI-calls)
            this.HandleChangesInSceneView();

            serializedObject.Update();
            bool changed = false;

            // draw the toggles for scene view windows
            CurveHelper.InspectorDrawWindowToggles();

            IEnumerable<CurveBase> curves = serializedObject.targetObjects.OfType<CurveBase>();
            CurveHelper.InspectorDrawUtilities(curves.ToArray<CurveBase>());


            EditorGUILayout.LabelField("Instance Data", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            {
                EditorGUILayout.PropertyField(propCurve);

                if (curveInst.Curve != null
                    && curveInst.Curve.GetType().Equals(typeof(Curve)))
                {
                    Curve curve = (Curve)curveInst.Curve;
                    curve.Type = (CurveTypes)EditorGUILayout.EnumPopup("Type", curve.Type);

                    CurveHelper.OptionCurveSegmentVerbosity = (CurveSegmentVerbosity)EditorGUILayout.EnumPopup("Segments", CurveHelper.OptionCurveSegmentVerbosity);
                    if (CurveHelper.OptionCurveSegmentVerbosity != CurveSegmentVerbosity.Off)
                        for (int i = 0; i < curve.NumberOfSegments; i++)
                            CurveEditor.InspectorDrawSegment(curve, i);
                }
            }
            if (EditorGUI.EndChangeCheck())
                changed = true;
            if (changed)
                this.HandleChanges();

            // check for shortcut inputs
            CurveHelper.CheckCurveEditorShortcuts();
        }


        void OnSceneGUI()
        {

            if (curveInst.Curve != null
                && curveInst.Curve.GetType().Equals(typeof(Curve)))
            {
                Curve curve = (Curve)curveInst.Curve;

                // register Undo point in case anything is changed on the curve
                Undo.RecordObject(curve, "Modify Curve");
                if (CurveHelper.SceneDrawCurve(curve, true, curve.IsTransition ? CurveHelper.ColorCurveTransition : CurveHelper.ColorCurve, 1.0f, curveInst.transform))
                    this.ChangedInSceneView = true;

                foreach (CurveTransitionInfo transition in curve.Transitions)
                {
                    if (transition.Curve is Curve)
                        if (CurveHelper.SceneDrawCurve(transition.Curve as Curve, true, CurveHelper.ColorCurveTransition))
                        {
                            Undo.RecordObject(transition.Curve, "Modify Curve");
                            EditorUtility.SetDirty(transition.Curve);
                        }
                }

                EditorUtility.SetDirty(curve);

                CurveEditorTools tool = (CurveEditorTools)CurveHelper.CurveEditorTool;
                if (Event.current.modifiers == EventModifiers.Shift)
                {
                    int segIndex = -1;
                    float segTime = -1.0f;
                    CurveEditor.ClosestSegment(curve, out segIndex, out segTime);

                    if (segIndex != -1 && segTime != -1.0f)
                    {
                        // draw the current active tool
                        CurveEditor.SceneDrawCurveEditorTool(curve, tool, segIndex, segTime);

                        if (Event.current.type == EventType.MouseUp)
                        {

                            if (Event.current.button == 1 &&
                                (tool == CurveEditorTools.Add || tool == CurveEditorTools.Remove
                                || tool == CurveEditorTools.Corner || tool == CurveEditorTools.Smooth)) // check for RMB and remove-tool
                                if (CurveEditor.UseCurveEditorTool(curve, tool, segIndex, segTime))
                                {
                                    EditorUtility.SetDirty(curve);
                                    CurveHelper.SceneViewRepaintAll();
                                    CurveHelper.InspectorRepaint(this);
                                }

                        }

                    }


                }

                // make sure the transform tool is not selected at the moment
                if (tool == CurveEditorTools.Pivot)
                {
                    if (UnityEditor.Tools.current != Tool.None)
                        UnityEditor.Tools.current = Tool.None;

                    Quaternion rotation = Quaternion.identity;
                    if (UnityEditor.Tools.pivotRotation == PivotRotation.Local)
                        rotation = curve.transform.rotation;

                    Vector3 delta = Handles.DoPositionHandle(curve.transform.position, rotation) - curve.transform.position;
                    curve.ModifyPivotTranslate(delta, false);
                }

                // draw scene view gui
                CurveHelper.DrawSceneGUI();
                /*
                if (Event.current.modifiers == EventModifiers.Shift)
                {
                    int segIndex = -1;
                    float segTime = -1.0f;
                    CurveEditor.ClosestSegment(curve, out segIndex, out segTime);

                    if (segIndex != -1 && segTime != -1.0f)
                    {
                        // draw the current active tool
                        CurveEditorTools tool = (CurveEditorTools)CurveHelper.CurveEditorTool;
                        CurveEditor.SceneDrawCurveEditorTool(curve, tool, segIndex, segTime);

                        if (Event.current.type == EventType.MouseUp)
                        {

                            if (Event.current.button == 1 &&
                                (tool == CurveEditorTools.Add || tool == CurveEditorTools.Remove
                                || tool == CurveEditorTools.Corner || tool == CurveEditorTools.Smooth)) // check for RMB and remove-tool
                                if (CurveEditor.UseCurveEditorTool(curve, tool, segIndex, segTime))
                                {
                                    EditorUtility.SetDirty(curve);
                                    this.Repaint();
                                    SceneView.RepaintAll();
                                }
                        }

                    }
                }

                // draw scene view gui
                CurveHelper.DrawSceneGUI();
                */
            }

        }

    }

}