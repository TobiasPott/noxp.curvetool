using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP
{
    [CanEditMultipleObjects()]
    [CustomEditor(typeof(Curve))]
    public class CurveEditor : SceneViewEditor
    {

        private static List<Vector3> _curveSegmentPoints = new List<Vector3>();

        private Curve curve = null;

        void OnEnable()
        {
            curve = ((Curve)serializedObject.targetObject);
            Undo.undoRedoPerformed += new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void Undo_UndoRedoPreformed()
        {
            if (curve != null)
            {
                foreach (CurveTransitionInfo transition in curve.Transitions)
                    if (transition.Curve != null)
                        CurveHelper.RefreshCurveTransition(transition.Curve.GetComponent<CurveTransition>(), true, false, false);
                curve.RefreshExtensions();
            }
            CurveHelper.SceneViewRepaintAll();
            CurveHelper.InspectorRepaint(this);
        }

        protected override void HandleChangesInSceneView()
        {
            if (!this.ChangedInSceneView)
                return;
            else
            {
                this.ChangedInSceneView = false;

                Undo.RecordObjects(this.targets, "Modify Curve");
                foreach (Curve other in this.targets)
                {
                    other.InitExtensions();
                    other.RefreshExtensions();

                    foreach (CurveTransitionInfo transition in other.Transitions)
                    {
                        if (transition.Curve != null)
                        {
                            CurveHelper.RefreshCurveTransition(transition.Curve.GetComponent<CurveTransition>(), true, false, false);
                            EditorUtility.SetDirty(transition.Curve);
                        }
                    }
                }
                CurveHelper.SceneViewRepaintAll();
                // handle general changes (which are also handled in OnInspectorGUI()-calls)
                this.HandleChanges();
            }

        }
        protected override void HandleChanges()
        {
            foreach (CurveTransitionInfo transition in curve.Transitions)
            {
                if (transition.Curve != null)
                {
                    CurveHelper.RefreshCurveTransition(transition.Curve.GetComponent<CurveTransition>(), true, false, false);
                    EditorUtility.SetDirty(transition.Curve);
                }
            }

            Undo.RegisterCompleteObjectUndo(curve, "Modify Curve");
            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(curve);
            CurveHelper.SceneViewRepaintAll();
        }

        public override void OnInspectorGUI()
        {
            // handle changes made in the scene view (mostly update stuff not accessible in OnSceneGUI-calls)
            this.HandleChangesInSceneView();

            serializedObject.Update();
            bool changed = false;

            // draw the toggles for scene view windows
            CurveHelper.InspectorDrawWindowToggles();

            IEnumerable<CurveBase> curves = serializedObject.targetObjects.OfType<CurveBase>();
            CurveHelper.InspectorDrawUtilities(curves.ToArray<CurveBase>());

            EditorGUILayout.LabelField("Curve Data", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            {
                // get the curve type and draw the property field
                SerializedProperty propType = this.serializedObject.FindProperty("_type");
                EditorGUILayout.PropertyField(propType);

                // draw foldout for segments
                CurveSegmentVerbosity verbosity = (CurveSegmentVerbosity)EditorGUILayout.EnumPopup("Segments", CurveHelper.OptionCurveSegmentFoldout ? CurveHelper.OptionCurveSegmentVerbosity : CurveSegmentVerbosity.Off);
                if (!CurveHelper.OptionCurveSegmentFoldout && verbosity != CurveSegmentVerbosity.Off)
                    CurveHelper.OptionCurveSegmentFoldout = true;
                if (verbosity == CurveSegmentVerbosity.Off)
                    CurveHelper.OptionCurveSegmentFoldout = false;
                else
                    CurveHelper.OptionCurveSegmentVerbosity = verbosity;

                Rect lastRect = GUILayoutUtility.GetLastRect();
                if (CurveHelper.OptionCurveSegmentFoldout && CurveHelper.OptionCurveSegmentVerbosity != CurveSegmentVerbosity.Off)
                {
                    for (int i = 0; i < curve.NumberOfSegments; i++)
                        using (new EditorGUILayout.HorizontalScope())
                            CurveEditor.InspectorDrawSegment(curve, i);
                }
                // draw foldout to provide easy hide/show for segments
                lastRect.width = EditorGUIUtility.labelWidth;
                CurveHelper.OptionCurveSegmentFoldout = EditorGUI.Foldout(lastRect, CurveHelper.OptionCurveSegmentFoldout, "");

            }
            if (EditorGUI.EndChangeCheck())
                changed = true;

            if (changed)
                this.HandleChanges();

            // check for shortcut inputs
            CurveHelper.CheckCurveEditorShortcuts();

        }

        public static void InspectorDrawSegment(Curve curve, int segIndex)
        {

            CurveSegment segment = curve[segIndex];
            using (new EditorGUILayout.VerticalScope())
            {
                float labelWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 50.0f;

                if (segIndex > 0 && curve.NumberOfSegments > 1)
                {
                    // drawn only at verbosity of 'tangents' or 'full'
                    if (CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Tangents
                        || CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Full)
                        if (curve[segIndex - 1].EndTangentMode != CurveTangentModes.Shared)
                            segment.StartTangent = EditorGUILayout.Vector3Field("Tan", segment.StartTangent);
                }
                else
                {
                    // drawn only at verbosity of 'tangents' or 'full'
                    if (CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Tangents
                    || CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Full)
                        segment.StartTangent = EditorGUILayout.Vector3Field("Tan", segment.StartTangent);
                    // drawn only at verbosity of 'points' or 'full'
                    if (CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Points
                        || CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Full)
                        segment.Start = EditorGUILayout.Vector3Field("CP", segment.Start);

                }
                //GUILayout.Space(2.0f);
                // drawn only at verbosity of 'minimal' or higher
                if (CurveHelper.OptionCurveSegmentVerbosity >= CurveSegmentVerbosity.Minimal)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        bool isLink = EditorGUILayout.Toggle("Link", segment.IsLink, GUILayout.ExpandWidth(false));
                        if (segment.IsLink != isLink)
                        {
                            segment.IsLink = isLink;
                            curve.RefreshConciseData();
                        }

                        CurveTangentModes mode = (CurveTangentModes)EditorGUILayout.EnumPopup(segment.EndTangentMode);
                        if (segment.EndTangentMode != mode)
                        {
                            segment.EndTangentMode = mode;
                        }

                        // provide the "focus" onto a specific segment
                        if (GUILayout.Button("Focus", EditorStyles.miniButton))
                            SceneView.lastActiveSceneView.LookAt(curve.transform.TransformPoint(Curve.Sample(segment, 0.5f, curve.Type)));
                    }
                }
                //GUILayout.Space(2.0f);

                // drawn only at verbosity of 'points' or 'full'
                if (CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Points
                || CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Full)
                    segment.End = EditorGUILayout.Vector3Field("CP", segment.End);
                // drawn only at verbosity of 'tangents' or 'full'
                if (CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Tangents
                    || CurveHelper.OptionCurveSegmentVerbosity == CurveSegmentVerbosity.Full)
                    segment.EndTangent = EditorGUILayout.Vector3Field("Tan", segment.EndTangent);

                EditorGUIUtility.labelWidth = labelWidth;
            }

            // GUILayout.Space(2.0f);

        }




        void OnSceneGUI()
        {
            Curve curve = (Curve)this.target;

            // register Undo point in case anything is changed on the curve
            Undo.RecordObject(curve, "Modify Curve");
            if (CurveHelper.SceneDrawCurve(curve, true, curve.IsTransition ? CurveHelper.ColorCurveTransition : CurveHelper.ColorCurve))
                this.ChangedInSceneView = true;

            foreach (CurveTransitionInfo transition in curve.Transitions)
            {
                if (transition.Curve is Curve)
                    if (CurveHelper.SceneDrawCurve(transition.Curve as Curve, true, CurveHelper.ColorCurveTransition))
                    {
                        Undo.RecordObject(transition.Curve, "Modify Curve");
                        EditorUtility.SetDirty(transition.Curve);
                    }
            }

            EditorUtility.SetDirty(curve);

            CurveEditorTools tool = (CurveEditorTools)CurveHelper.CurveEditorTool;
            if (Event.current.modifiers == EventModifiers.Shift)
            {
                int segIndex = -1;
                float segTime = -1.0f;
                CurveEditor.ClosestSegment(curve, out segIndex, out segTime);

                if (segIndex != -1 && segTime != -1.0f)
                {
                    // draw the current active tool
                    CurveEditor.SceneDrawCurveEditorTool(curve, tool, segIndex, segTime);

                    if (Event.current.type == EventType.MouseUp)
                    {

                        if (Event.current.button == 1 &&
                            (tool == CurveEditorTools.Add || tool == CurveEditorTools.Remove
                            || tool == CurveEditorTools.Corner || tool == CurveEditorTools.Smooth)) // check for RMB and remove-tool
                            if (CurveEditor.UseCurveEditorTool(curve, tool, segIndex, segTime))
                            {
                                EditorUtility.SetDirty(curve);
                                CurveHelper.SceneViewRepaintAll();
                                CurveHelper.InspectorRepaint(this);
                            }

                    }

                }


            }

            // make sure the transform tool is not selected at the moment
            if (tool == CurveEditorTools.Pivot)
            {
                if (UnityEditor.Tools.current != Tool.None)
                    UnityEditor.Tools.current = Tool.None;

                Quaternion rotation = Quaternion.identity;
                if (UnityEditor.Tools.pivotRotation == PivotRotation.Local)
                    rotation = curve.transform.rotation;

                Vector3 delta = Handles.DoPositionHandle(curve.transform.position, rotation) - curve.transform.position;
                curve.ModifyPivotTranslate(delta, false);
            }

            // draw scene view gui
            CurveHelper.DrawSceneGUI();

            CurveHelper.ProcessCurveEdit(SceneView.lastActiveSceneView.pivot, SceneView.lastActiveSceneView.camera);

        }

        public static void SceneDrawCurveEditorTool(Curve curve, CurveEditorTools tool, int segment, float time)
        {
            if (CurveEditor.CheckCurveEditorToolUse(tool, curve, segment))
            {
                Vector3 pos = curve.transform.TransformPoint(Curve.Sample(curve[segment], time, curve.Type));
                Vector3 posEnd = curve.transform.TransformPoint(Curve.Sample(curve[segment], time + 0.005f, curve.Type));
                Vector3 toolDir = (posEnd - pos).normalized;
                Vector3 toolViewNml = Vector3.Cross(toolDir, SceneView.currentDrawingSceneView.camera.transform.forward);
                float hSize = HandleUtility.GetHandleSize(pos);

                Color hColor = Handles.color;

                if (tool != CurveEditorTools.Edit)
                    Handles.Label(pos - toolViewNml * hSize * 0.125f, "   " + tool.ToString(), EditorStyles.whiteLargeLabel);

                switch (tool)
                {
                    case CurveEditorTools.Add:
                        hSize *= 0.5f;
                        Handles.color = Color.red;
                        Handles.DrawAAPolyLine(3.0f, (pos + toolViewNml * hSize), (pos - toolViewNml * hSize));
                        //Handles.DrawDottedLine(pos + toolViewNml * hSize, pos - toolViewNml * hSize, 3);
                        break;
                    case CurveEditorTools.Remove:
                        hSize *= 0.125f;
                        if (time > 0.15f && time < 0.85f) // check if time is in "invalid" range to avoid accidential deletion of segments
                            Handles.color = Color.gray;
                        else if (curve.NumberOfSegments == 1)
                            Handles.color = Color.gray;
                        //else if ((segment == 0 && time < 0.85f)
                        //    || (segment == curve.NumberOfSegments - 1 && time > 0.15f))
                        //    Handles.color = Color.gray;
                        else
                            Handles.color = Color.red;

                        Handles.DrawWireDisc(pos, SceneView.currentDrawingSceneView.camera.transform.forward, hSize);
                        Handles.DrawWireDisc(pos, SceneView.currentDrawingSceneView.camera.transform.forward, hSize * 0.9f);
                        break;
                    case CurveEditorTools.Corner:
                    case CurveEditorTools.Smooth:
                        hSize *= 0.125f;
                        if (time > 0.15f && time < 0.85f) // check if time is in "invalid" range to avoid accidential deletion of segments
                            Handles.color = Color.gray;
                        else
                        {
                            if (tool == CurveEditorTools.Corner) Handles.color = new Color(0.0f, 0.75f, 0.75f);
                            else if (tool == CurveEditorTools.Smooth) Handles.color = new Color(0.0f, 0.75f, 0.75f);
                        }

                        Handles.DrawWireDisc(pos, SceneView.currentDrawingSceneView.camera.transform.forward, hSize);
                        Handles.DrawWireDisc(pos, SceneView.currentDrawingSceneView.camera.transform.forward, hSize * 0.8f);
                        break;
                }
                Handles.color = hColor;

            }
        }
        public static bool UseCurveEditorTool(Curve curve, CurveEditorTools tool, int segment, float time)
        {
            if (CurveEditor.CheckCurveEditorToolUse(tool, curve, segment))
            {
                switch (tool)
                {
                    case CurveEditorTools.Add:
                        if (segment <= -1 && segment >= curve.NumberOfSegments)
                            return false;
                        curve.SplitSegment(segment, time);
                        return true;

                    case CurveEditorTools.Remove:
                        if (time > 0.15f && time < 0.85f) // check if time is in "invalid" range to avoid accidential deletion of segments
                            return false;
                        if (curve.NumberOfSegments == 1)
                            return false;
                        //if (((segment == 0 && time < 0.85f) ||
                        //    (segment == curve.NumberOfSegments - 1 && time > 0.15f)))
                        //    return false;

                        curve.DeleteSegment(segment, time > 0.5f ? CurveSegmentParts.End : CurveSegmentParts.Start);
                        return true;
                    case CurveEditorTools.Corner:
                    case CurveEditorTools.Smooth:
                        if (time > 0.15f && time < 0.85f) // check if time is in "invalid" range to avoid accidential deletion of segments
                            return false;

                        if (tool == CurveEditorTools.Corner) curve.MakeCurvePointCorner(segment, time > 0.5f ? CurveSegmentParts.End : CurveSegmentParts.Start);
                        else if (tool == CurveEditorTools.Smooth) curve.MakeCurvePointSmooth(segment, time > 0.5f ? CurveSegmentParts.End : CurveSegmentParts.Start);
                        return true;

                }
            }

            return false;
        }
        private static bool CheckCurveEditorToolUse(CurveEditorTools tool, Curve curve, int segmentIndex)
        {
            //if ((tool == CurveEditorTools.Remove && (segmentIndex >= 1 && segmentIndex < curve.Segments.Count - 1)) // check for remove tool and valid index range
            if (segmentIndex >= 0 && segmentIndex < curve.NumberOfSegments) // check for split tool and valid index range
                return true;
            return false;
        }

        public static void ClosestSegment(Curve curve, out int index, out float time)
        {
            //Ray r = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            float[] distToSegments = new float[curve.NumberOfSegments];
            Vector3[] closestPointsToSegment = new Vector3[curve.NumberOfSegments];

            for (int i = 0; i < curve.NumberOfSegments; i++)
            {
                // init distance with -1 to signalize invaild/undefined value
                _curveSegmentPoints.Clear();
                // add a dynamic amount of segment points to the list (to increase the precision on finding the point close to the bezier curve
                _curveSegmentPoints.Add(curve.transform.TransformPoint(curve[i].Start));
                _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[i], 0.2f, curve.Type)));
                _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[i], 0.4f, curve.Type)));
                _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[i], 0.6f, curve.Type)));
                _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[i], 0.8f, curve.Type)));
                _curveSegmentPoints.Add(curve.transform.TransformPoint(curve[i].End));

                closestPointsToSegment[i] = CurveEditor.ClosestPointToPolyLine(_curveSegmentPoints);
                distToSegments[i] = (HandleUtility.WorldToGUIPoint(closestPointsToSegment[i]) - Event.current.mousePosition).magnitude;

                // DEBUG
                //Handles.DrawAAPolyLine(2, _curveSegmentPoints.ToArray());
                //Handles.color = new Color(1, 0, 1);
                //Handles.DrawAAPolyLine(5, closestPointsToSegment[i], HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin);
            }

            index = Array.IndexOf(distToSegments, Mathf.Min(distToSegments));
            time = -1.0f;

            _curveSegmentPoints.Clear();
            // add a dynamic amount of segment points to the list (to increase the precision on finding the point close to the bezier curve
            _curveSegmentPoints.Add(curve.transform.TransformPoint(curve[index].Start));
            _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[index], 0.2f, curve.Type)));
            _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[index], 0.4f, curve.Type)));
            _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[index], 0.6f, curve.Type)));
            _curveSegmentPoints.Add(curve.transform.TransformPoint(Curve.Sample(curve[index], 0.8f, curve.Type)));
            _curveSegmentPoints.Add(curve.transform.TransformPoint(curve[index].End));


            float segLength = 1.0f / (_curveSegmentPoints.Count - 1);
            for (int j = 0; j < _curveSegmentPoints.Count - 1; j++)
            {
                Vector3 line = _curveSegmentPoints[j + 1] - _curveSegmentPoints[j];

                Plane pStart = new Plane(line.normalized, _curveSegmentPoints[j]);
                Plane pEnd = new Plane(-line.normalized, _curveSegmentPoints[j + 1]);
                if (!pStart.GetSide(closestPointsToSegment[index])
                    || !pEnd.GetSide(closestPointsToSegment[index]))
                    continue;

                float distToSegmentStep = ((closestPointsToSegment[index] - _curveSegmentPoints[j]).magnitude / (line).magnitude) * segLength;
                float distSegStepBase = j * segLength;
                time = distSegStepBase + distToSegmentStep;
                break; // break to take the first valid result
            }

        }

        /// <summary>
        ///   <para>Get the point on a polyline (in 3D space) which is closest to the current mouse position.</para>
        /// </summary>
        /// <param name="vertices"></param>
        public static Vector3 ClosestPointToPolyLine(List<Vector3> vertices)
        {
            float distMin = HandleUtility.DistanceToLine(vertices[0], vertices[1]);
            int num = 0;
            for (int i = 2; i < vertices.Count; i++)
            {
                float distToLine = HandleUtility.DistanceToLine(vertices[i - 1], vertices[i]);
                if (distToLine < distMin)
                {
                    distMin = distToLine;
                    num = i - 1;
                }
            }

            Vector3 lineStart = vertices[num];
            Vector3 vector31 = vertices[num + 1];
            Vector2 gUIPoint = Event.current.mousePosition - HandleUtility.WorldToGUIPoint(lineStart);
            Vector2 vector2 = HandleUtility.WorldToGUIPoint(vector31) - HandleUtility.WorldToGUIPoint(lineStart);
            float single1 = vector2.magnitude;
            float single2 = Vector3.Dot(vector2, gUIPoint);
            if (single1 > 1E-06f)
            {
                single2 = single2 / (single1 * single1);
            }
            single2 = Mathf.Clamp01(single2);
            return Vector3.Lerp(lineStart, vector31, single2);
        }


    }

}