﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace NoXP
{
    //[CanEditMultipleObjects()]
    [CustomEditor(typeof(CurveRetime))]
    public class CurveRetimeEditor : Editor
    {

        private SerializedProperty propRetimeMode;
        private SerializedProperty propQuality;
        private SerializedProperty propAdjustmentCurve;


        private void OnEnable()
        {
            propRetimeMode = serializedObject.FindProperty("_retimeMode");
            propQuality = serializedObject.FindProperty("_quality");
            propAdjustmentCurve = serializedObject.FindProperty("_adjustmentCurve");

            CurveRetime retime = ((CurveRetime)serializedObject.targetObject);
            retime.GetComponent<Curve>().RegisterExtension(retime, true);
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            CurveHelper.InspectorRepaint(this);

            bool changed = false;
            bool propChanged = false;
            EditorGUI.BeginChangeCheck();
            { EditorGUILayout.PropertyField(propRetimeMode); }
            if (EditorGUI.EndChangeCheck())
                propChanged = changed = true;
            CurveRetimeModes mode = (CurveRetimeModes)propRetimeMode.intValue;
            if (mode == CurveRetimeModes.Adjusted)
            {
                EditorGUI.BeginChangeCheck();
                {
                    EditorGUILayout.PropertyField(propQuality);
                    EditorGUILayout.CurveField("Adjustment", propAdjustmentCurve.animationCurveValue);
                }
                if (EditorGUI.EndChangeCheck())
                    propChanged = changed = true;

                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.PrefixLabel(" ");
                    if (GUILayout.Button("Refresh"))
                        changed = true;
                }

            }

            if (propChanged)
                serializedObject.ApplyModifiedProperties();
            if (changed)
            {
                Undo.RecordObjects(serializedObject.targetObjects, "Modify Bezier Curve Retime");
                foreach (CurveRetime retime in serializedObject.targetObjects)
                {
                    //retime.Curve.RefreshConciseData();
                    retime.Rebuild();
                    EditorUtility.SetDirty(retime);
                }

                serializedObject.ApplyModifiedProperties();
            }

        }

    }

}
