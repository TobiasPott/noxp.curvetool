﻿using UnityEditor;
using UnityEngine;


namespace NoXP
{
    public abstract class SceneViewEditor : Editor
    {
        private bool _changedInSceneGUI = false;
        protected bool ChangedInSceneView
        {
            get { return _changedInSceneGUI; }
            set { _changedInSceneGUI = value; }
        }


        protected abstract void HandleChangesInSceneView();
        protected abstract void HandleChanges();
    }


    //[CanEditMultipleObjects()]
    [CustomEditor(typeof(CurveTransition))]
    public class CurveTransitionEditor : SceneViewEditor
    {

        //private SerializedProperty propData;
        private SerializedProperty propSource;
        private SerializedProperty propTarget;
        private SerializedProperty propSourceActive;
        private SerializedProperty propTargetActive;
        private SerializedProperty propTimeSource;
        private SerializedProperty propTimeTarget;

        private CurveTransition transition;
        private Curve curve;
        private bool _prevChanged = false;


        private void OnEnable()
        {
            propSourceActive = serializedObject.FindProperty("_sourceActive");
            propTargetActive = serializedObject.FindProperty("_targetActive");
            propSource = serializedObject.FindProperty("_source");
            propTarget = serializedObject.FindProperty("_target");
            propTimeSource = serializedObject.FindProperty("_sourceTime");
            propTimeTarget = serializedObject.FindProperty("_targetTime");

            transition = ((CurveTransition)serializedObject.targetObject);
            curve = transition.GetComponent<Curve>();
            curve.InitExtensions();
            curve.RefreshExtensions();

            CurveHelper.RefreshCurveTransition(transition, true, true, true);
            CurveHelper.SceneViewRepaintAll();
            CurveHelper.InspectorRepaint(this);

            Undo.undoRedoPerformed += new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= new Undo.UndoRedoCallback(Undo_UndoRedoPreformed);
        }

        private void Undo_UndoRedoPreformed()
        {
            if (transition != null)
                transition.Refresh();
            
            CurveHelper.SceneViewRepaintAll();
            CurveHelper.InspectorRepaint(this);
        }


        protected override void HandleChangesInSceneView()
        {
            if (!this.ChangedInSceneView)
                return;
            else
            {
                this.ChangedInSceneView = false;
                // handle general changes (which are also handled in OnInspectorGUI()-calls)
                this.HandleChanges();
            }
        }
        protected override void HandleChanges()
        {
            Undo.RecordObject(serializedObject.targetObject, "Modify Curve Transition");

            // refresh the transition's curve 
            transition.Refresh();
            serializedObject.ApplyModifiedProperties();
            CurveHelper.SceneViewRepaintAll();
            CurveHelper.InspectorRepaint(this);
        }


        public override void OnInspectorGUI()
        {
            // handle changes made in the scene view (mostly update stuff not accessible in OnSceneGUI-calls)
            this.HandleChangesInSceneView();

            serializedObject.Update();
            bool changed = false;
            // ! ! !
            // do separate ChangedCheck for Source and Target property to reduce the curves 'RefreshExtension' calls load
            //  ->  might generate to much load when each slider change updates both curves extensions (with retime only at the moment and looking into the future with more data-heavy extensions)
            //  ->  put buttons beside Source & Target to refresh them manually?!
            EditorGUI.BeginChangeCheck();
            {
                EditorGUILayout.PropertyField(propSource);
                transition.SourceActive = EditorGUILayout.Toggle(propSourceActive.displayName, propSourceActive.boolValue);
                EditorGUILayout.PropertyField(propTimeSource);
            }
            if (EditorGUI.EndChangeCheck())
                changed = true;

            EditorGUI.BeginChangeCheck();
            {
                EditorGUILayout.PropertyField(propTarget);
                transition.TargetActive = EditorGUILayout.Toggle(propTargetActive.displayName, propTargetActive.boolValue);
                EditorGUILayout.PropertyField(propTimeTarget);
            }
            if (EditorGUI.EndChangeCheck())
                changed = true;

            if (changed)
            {
                Undo.RecordObject(serializedObject.targetObject, "Modify Curve Transition");
                // refresh the transition's curve 
                CurveHelper.RefreshCurveTransition(transition, true, true, true);
                transition.Refresh();
                serializedObject.ApplyModifiedProperties();
                CurveHelper.SceneViewRepaintAll();
                CurveHelper.InspectorRepaint(this);
            }

            // only update the curve on the transition itself (update all others should not be required as they are never changed)
            if (_prevChanged && !changed)
            {
                CurveHelper.RefreshCurveTransition(transition, true, false, false);
                transition.Refresh();
            }
            _prevChanged = changed;

        }

        void OnSceneGUI()
        {
            // draw the source & target curves of the transition
            // null check is done inside the method
            if (transition.Source is Curve)
            {
                Curve source = transition.Source as Curve;
                if (CurveHelper.SceneDrawCurve(source, true, CurveHelper.ColorCurve, 0.75f))
                {
                    Undo.RegisterCompleteObjectUndo(transition.Source, "Modify Curve");
                    source.RefreshExtensions();
                    transition.Refresh();
                    CurveHelper.SceneViewRepaintAll();
                }
            }

            if (transition.Target is Curve)
            {
                Curve target = transition.Target as Curve;
                // null check is done inside the method
                if (CurveHelper.SceneDrawCurve(target, true, CurveHelper.ColorCurve, 0.75f))
                {
                    Undo.RegisterCompleteObjectUndo(transition.Target, "Modify Curve");
                    target.RefreshExtensions();
                    transition.Refresh();
                    CurveHelper.SceneViewRepaintAll();
                }
            }

            Event ev = Event.current;
            if (ev.type == EventType.MouseDrag
                || ev.type == EventType.MouseUp)
            {
                this.ChangedInSceneView = true;
            }

        }



    }
}
