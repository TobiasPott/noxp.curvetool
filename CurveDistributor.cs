﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NoXP
{
    [RequireComponent(typeof(CurveBase))]
    public class CurveDistributor : MonoBehaviour
    {

        public enum DistributionModes
        {
            Distance,        // based on distance
            Time,        // based on time
        }

        [SerializeField()]
        private DistributionModes _mode = DistributionModes.Distance;

        [SerializeField()]
        private float _spacing = 0.1f;

        [SerializeField()]
        private GameObject[] _prefabs = new GameObject[0];

        [HideInInspector()]
        [SerializeField()]
        private List<GameObject> _distributed = new List<GameObject>();


        public DistributionModes Mode
        { get { return _mode; } }
        public float Spacing
        { get { return _spacing; } }
        public GameObject[] Prefabs
        { get { return _prefabs; } }
        public List<GameObject> Distributed
        { get { return _distributed; } }

    }




}