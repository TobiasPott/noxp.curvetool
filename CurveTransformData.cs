﻿using UnityEngine;
using System;

namespace NoXP
{
    [Serializable()]
    public class CurveTransformData
    {
        private static Vector3 Vector3Min = new Vector3(float.MinValue, float.MinValue, float.MinValue);

        // flag to enable/disable evaluation
        [SerializeField()]
        private bool _enabled = true;
        [SerializeField()]
        private CurveTransformOptions _options = new CurveTransformOptions(true, false, true, true);

        private float _time = 0.0f;
        [SerializeField()]
        private CurveTransformPace _pace = CurveTransformPace.Speed;
        [SerializeField()]
        private float _duration = 1.0f;
        [SerializeField()]
        private float _speed = 5.0f; // measured in units per second
        // ! ! ! !
        // not yet the ideal way to implement the offset as it does not get applied to CurveTransformData automatically and have to be set wherever an instance of the class is used.
        [SerializeField()]
        private float _timeOffset = 0.0f; // measured in normalized time (0-1)
        [SerializeField()]
        private Vector3 _localOffset = Vector3.zero; // measured in units per second


        private float _lastTimeTransited = 0;
        private float _lastTimeTransitedUp = 0;
        private float _timeTransitedYield = 0.5f; // results in same values as with frame count but feels more appropiate to remove frame-rate dependencies

        private Vector3 _prevPos = CurveTransformData.Vector3Min;
        private Vector3 _prevOffset = Vector3.zero;


        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }
        public CurveTransformOptions Options
        { get { return _options; } }
        public float CurveTime
        {
            get { return _time; }
            set { _time = value; }
        }
        public CurveTransformPace Pace
        {
            get { return _pace; }
            set { _pace = value; }
        }

        public float Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public float TimeOffset
        {
            get { return _timeOffset; }
            set { _timeOffset = value; }
        }

        public Vector3 LocalOffset
        {
            get { return _localOffset; }
            set { _localOffset = value; }
        }


        public void Evaluate(Transform target, ref CurveBase curve)
        { this.Evaluate(target, null, ref curve); }
        public void Evaluate(Rigidbody target, ref CurveBase curve)
        { this.Evaluate(null, target, ref curve); }
        public void Evaluate(Transform tTransform, Rigidbody tRigidbody, ref CurveBase curve)
        {
            if (!_enabled)
                return;
            if (tRigidbody == null && tTransform == null)
                return;

            if (_prevPos == CurveTransformData.Vector3Min)
                _prevPos = tTransform != null ? tTransform.position : tRigidbody.position;

            if (!_options.Loop && (_time > 1.0f || _time < 0.0f))
            {
                _enabled = false;
                _time = Mathf.Clamp(_time, 0.0f, 1.0f);
                return;
            }

            // increase time for the CurveTransform
            _time += (_options.Reverse ? -Time.deltaTime : Time.deltaTime) * this.GetInvDurationFromPace(curve);

            if (_options.Loop)
            {
                if (_time > 1.0f)
                    _time -= 1.0f;
                else if (_time < 0.0f)
                    _time += 1.0f;
            }

            Vector3 targetPosition = tTransform != null ? tTransform.position : tRigidbody.position;
            Vector3 targetUp = tTransform != null ? tTransform.up : tRigidbody.transform.up;

            if (curve != null)
            {
                if (_options.FollowTransition && _lastTimeTransited < (Time.unscaledTime - _timeTransitedYield))
                {
                    if (curve.HasTransition(ref _time, ref curve))
                        _lastTimeTransited = Time.unscaledTime;
                }
                targetPosition = curve.Position(_time);
                targetUp = curve.transform.up;
                // ! ! ! ! 
                // add a function on the CurveBase class which takes a position as input
                //  and fills in an up, forward and right vector (or directly manipulates a transform) 


                if (_options.OrientToCurve)
                {
                    Vector3 fwd = ((targetPosition) - _prevPos).normalized;
                    if (fwd != Vector3.zero)
                    {
                        if (tTransform != null)
                            tTransform.rotation = Quaternion.LookRotation(fwd, targetUp);
                        else
                            tRigidbody.MoveRotation(Quaternion.LookRotation(fwd, targetUp));
                    }
                }

                if (tTransform != null)
                {
                    if (_localOffset != Vector3.zero)
                        tTransform.position = targetPosition + tTransform.TransformVector(_localOffset);
                    else
                        tTransform.position = targetPosition;
                }
                else
                {
                    if (_localOffset != Vector3.zero)
                        tRigidbody.position = targetPosition + tRigidbody.transform.TransformVector(_localOffset);
                    else
                        tRigidbody.position = targetPosition;
                }

                _prevPos = targetPosition;
            }
        }

        public void Evaluate(Transform target, ref CurveBase curve, ref CurveBase curveUp)
        {
            if (curveUp != null)
                this.Evaluate(target, null, ref curve, ref curveUp);
            else
                this.Evaluate(target, null, ref curve);
        }
        public void Evaluate(Rigidbody target, ref CurveBase curve, ref CurveBase curveUp)
        {
            if (curveUp != null)
                this.Evaluate(null, target, ref curve, ref curveUp);
            else
                this.Evaluate(null, target, ref curve);
        }
        public void Evaluate(Transform tTransform, Rigidbody tRigidbody, ref CurveBase curve, ref CurveBase curveUp)
        {
            if (!_enabled)
                return;
            if (tRigidbody == null && tTransform == null)
                return;

            if (_prevPos == CurveTransformData.Vector3Min)
                _prevPos = tTransform != null ? tTransform.position : tRigidbody.position;

            if (!_options.Loop && (_time > 1.0f || _time < 0.0f))
            {
                _enabled = false;
                _time = Mathf.Clamp(_time, 0.0f, 1.0f);
                return;
            }


            // increase time for the CurveTransform
            _time += (_options.Reverse ? -Time.deltaTime : Time.deltaTime) * this.GetInvDurationFromPace(curve);

            if (_options.Loop)
            {
                if (_time > 1.0f)
                    _time -= 1.0f;
                else if (_time < 0.0f)
                    _time += 1.0f;
            }

            Vector3 targetPosition = tTransform != null ? tTransform.position : tRigidbody.position;
            Vector3 targetUp = tTransform != null ? tTransform.up : tRigidbody.transform.up;
            if (curve != null)
            {
                if (_options.FollowTransition && _lastTimeTransited < (Time.unscaledTime - _timeTransitedYield))
                {
                    if (curve.HasTransition(ref _time, ref curve))
                        _lastTimeTransited = Time.unscaledTime;
                }
                targetPosition = curve.Position(_time);
                // ! ! ! ! 
                // add a function on the CurveBase class which takes a position as input
                //  and fills in an up, forward and right vector (or directly manipulates a transform) 
            }
            if (curveUp != null)
            {
                if (_options.FollowTransition && _lastTimeTransitedUp < (Time.unscaledTime - _lastTimeTransitedUp))
                {
                    if (curveUp.HasTransition(ref _time, ref curveUp))
                        _lastTimeTransitedUp = Time.unscaledTime;
                }
                targetUp = (curveUp.Position(_time) - targetPosition).normalized;
            }


            if (_options.OrientToCurve)
            {
                Vector3 fwd = ((targetPosition) - _prevPos).normalized;
                if (fwd != Vector3.zero)
                {
                    if (tTransform != null)
                        tTransform.rotation = Quaternion.LookRotation(fwd, targetUp);
                    else
                        tRigidbody.MoveRotation(Quaternion.LookRotation(fwd, targetUp));
                }
            }


            if (tTransform != null)
            {
                if (_localOffset != Vector3.zero)
                    tTransform.position = targetPosition + tTransform.TransformVector(_localOffset);
                else
                    tTransform.position = targetPosition;
            }
            else
            {
                if (_localOffset != Vector3.zero)
                    tRigidbody.position = targetPosition + tRigidbody.transform.TransformVector(_localOffset);
                else
                    tRigidbody.position = targetPosition;
            }

            _prevPos = targetPosition;
        }


        private float GetInvDurationFromPace(ICurve curve)
        {
            if (_pace == CurveTransformPace.Speed)
                return 1.0f / (curve.Length / _speed);
            else if (_pace == CurveTransformPace.Duration)
                return 1.0f / _duration;
            return 1.0f;
        }

        private float GetSpeedToDurationRatio(ICurve curve)
        { return (curve.Length / _speed) / _duration; }
        private float GetDurationToSpeedRatio(ICurve curve)
        { return 1.0f / GetSpeedToDurationRatio(curve); }


        public void Validate()
        {
            // clamp time to the valid range of 0 - 1
            _time = Mathf.Clamp(_time, 0.0f, 1.0f);
        }

        public void Reset(float time = 0.0f)
        {
            _time = time;
            _time = _timeOffset + time;
            _lastTimeTransited = 0.0f;
            _prevPos = CurveTransformData.Vector3Min;
        }

        public void CopyFrom(CurveTransformData source, bool allData = false)
        {
            this._pace = source._pace;
            this._duration = source._duration;
            this._speed = source._speed;
            this._timeOffset = source._timeOffset;
            this._localOffset = source._localOffset;
            this._options.SetLoop(source._options.Loop);
            this._options.SetReverse(source._options.Reverse);
            this._options.SetOrientToCurve(source._options.OrientToCurve);
            this._options.SetFollowTransition(source._options.FollowTransition);
            if (allData)
            {
                this._lastTimeTransited = source._lastTimeTransited;
                this._lastTimeTransitedUp = source._lastTimeTransitedUp;
                this._timeTransitedYield = source._timeTransitedYield;
                this._prevPos = source._prevPos;
                this._prevOffset = source._prevOffset;
                this._time = source._time;
            }
        }

    }

}