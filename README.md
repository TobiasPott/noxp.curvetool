# CURVETOOL #
![Alt text](https://bytebucket.org/TobiasPott/noxp.curvetool/raw/2599d1a84f8a3523baeb8c60800440e07b15cb59/Samples/Bitbucket.jpg?token=257ae11d8aabe3a4a66366ffbbb9de00b2bdd19b "Optional title")

### What is it? ###

The NoXP.CurveTool repository is a collection of run-time and editor scripts for Unity game engine. It originated in my bachelor-degree project Lemhach (www.tobiaspott.de/lemhach) and was used for creating and designing movement paths for enemy groups.

The tool allows to use linear and bezier type of curves and create them either on a numerical base, point by point or similar to Illustrator by point, click & drag a curves points. It allows adding and removing control points after curve creation and smooth or sharp corners (on bezier).
Part of the tool are also:

* curve instances, which share curve data but transform according to the instances world-space transformation
* curve transitions, which allow custom transition between curves to build more complex path systems with branching and merging
* curve blend, which allows the blend between two other curves (e.g. changing a camera movement path with a single 0-1 slider instead of a lot of custom code
* curve retime, which allows interpretation of a curve segments to be more accurate depending on their length in relation to the complete curves length
* curve remap, which allows manipulation of a curve based on a custom input 2D curve, which remaps the time parameter for a curve based on the input curve.
* demo scripts for curve transformation to transform objects or rigidbodies along a curve, distribution to fast distribute objects along a curve based on time or distance

... and a list of stuff still left to do for this tool. =)

### And how do I get set up? ###

* clone or download the repository and put it's content somewhere in your Unity project. The UI and editor parts should auto-detect paths and load themselves properly.
* create a new curve object (via: GameObject -> 3D Object -> Curve; or by adding the "Curve" component to an existing object. This will create a simple two-point curve in your scene which you can begin with.
* open the "Curve Tools" from the inspector and start editing your curve.

### Additional steps ###

Unfortunately my focus is on developing tools and less on documentation at the moment, but I do my best to keep it updated and help anyone interested to understand my tool, use it and extend it.

* Take a look at the "Samples/CurveDev.unity" scene to see the base setup of a linear, bezier and a transition between two curves. The scene also contains some transformer objects which 'ride' along their curves with different options set (e.g. looping, orientation, transition, duration/pace, time offset or position offset.
It also contains a distribution example for placing a box template along a curve in a specified distance from another. The distribution example also uses link-segments which are edited like normal segments of a curve but aren't considered on evaluation of a curve (transformers or distribution leave out the area) and can be toggled link/non-link whenever it is needed.


### Who do I talk to? ###

* Just send me a message or stop by my website: www.tobiaspott.de