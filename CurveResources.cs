﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

namespace NoXP
{
    // ! ! ! !
    // does not need to reside
#if UNITY_EDITOR
    public class CurveResources
    {
        public const string ComponentMenuBase = "NoXP/Frameworks/Curve";

        private static string EditorDefaultResources = "Editor Default Resources/";
        private static string _basePath = "Assets/NoXP/Frameworks/Curve";
        private static string SubPath_UI = "/UI";

        private static Type TCurveResources = typeof(CurveResources);

        public static string BasePath
        {
            get
            {
                string[] assetPaths = AssetDatabase.FindAssets("t:Script " + TCurveResources.Name);
                if (assetPaths.Length > 0)
                {
                    string assetPath = AssetDatabase.GUIDToAssetPath(assetPaths[0]).Replace("/" + TCurveResources.Name + ".cs", "");
                    if (!_basePath.Equals(assetPath))
                        _basePath = assetPath;
                    Debug.Log(_basePath);
                }
                return _basePath;
            }
        }


        public static readonly string[] ShelvesNamesFiller = new string[] { "" };

        public static T LoadAsset<T>(string path, bool editorDefaultResources = false) where T : UnityEngine.Object
        {
            if (editorDefaultResources)
                return (T)EditorGUIUtility.Load(path);
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);

        }
        public static T LoadAssetAuto<T>(string path) where T : UnityEngine.Object
        {
            if (path.StartsWith(EditorDefaultResources))
                return (T)EditorGUIUtility.Load(path.Substring(EditorDefaultResources.Length));
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);

        }

        public enum CurveToolTextures
        {
            // main curve tools
            Cursor,
            Add,
            Remove,
            Corner,
            Smooth,
            Pivot,
            // pencil/editing
            Pencil,
            PencilAdd,
            PencilRemove,
            PencilCancel,
            // other/misc tools or utilities
            Curve,
            Reverse,
            Help,
            OptionGizmo,
            OptionPoints,
            OptionTangents,
            Empty
        }

        private static Dictionary<CurveToolTextures, Texture2D> _textures = new Dictionary<CurveToolTextures, Texture2D>();
        public static Texture2D UIRES_CurveToolCursor
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Cursor))
                    _textures.Add(CurveToolTextures.Cursor, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_CurveTool_Cursor.png"));
                return _textures[CurveToolTextures.Cursor];
            }
        }
        public static Texture2D UIRES_CurveToolAdd
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Add))
                    _textures.Add(CurveToolTextures.Add, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Add.png"));
                return _textures[CurveToolTextures.Add];
            }
        }
        public static Texture2D UIRES_CurveToolRemove
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Remove))
                    _textures.Add(CurveToolTextures.Remove, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Remove.png"));
                return _textures[CurveToolTextures.Remove];
            }
        }
        public static Texture2D UIRES_CurveToolCorner
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Corner))
                    _textures.Add(CurveToolTextures.Corner, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Corner.png"));
                return _textures[CurveToolTextures.Corner];
            }
        }
        public static Texture2D UIRES_CurveToolSmooth
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Smooth))
                    _textures.Add(CurveToolTextures.Smooth, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Smooth.png"));
                return _textures[CurveToolTextures.Smooth];
            }
        }
        public static Texture2D UIRES_CurveToolPivot
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Pivot))
                    _textures.Add(CurveToolTextures.Pivot, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Pivot.png"));
                return _textures[CurveToolTextures.Pivot];
            }
        }

        public static Texture2D UIRES_CurveToolPencil
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Pencil))
                    _textures.Add(CurveToolTextures.Pencil, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Pencil.png"));
                return _textures[CurveToolTextures.Pencil];
            }
        }
        public static Texture2D UIRES_CurveToolPencilAdd
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.PencilAdd))
                    _textures.Add(CurveToolTextures.PencilAdd, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_PencilAdd.png"));
                return _textures[CurveToolTextures.PencilAdd];
            }
        }
        public static Texture2D UIRES_CurveToolPencilCancel
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.PencilCancel))
                    _textures.Add(CurveToolTextures.PencilCancel, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_PencilCancel.png"));
                return _textures[CurveToolTextures.PencilCancel];
            }
        }
        public static Texture2D UIRES_CurveToolPencilRemove
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.PencilRemove))
                    _textures.Add(CurveToolTextures.PencilRemove, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_PencilRemove.png"));
                return _textures[CurveToolTextures.PencilRemove];
            }
        }


        public static Texture2D UIRES_CurveToolCurve
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Curve))
                    _textures.Add(CurveToolTextures.Curve, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Curve.png"));
                return _textures[CurveToolTextures.Curve];
            }
        }
        public static Texture2D UIRES_CurveToolHelp
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Help))
                    _textures.Add(CurveToolTextures.Help, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Help.png"));
                return _textures[CurveToolTextures.Help];
            }
        }

        public static Texture2D UIRES_CurveToolOptionGizmo
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.OptionGizmo))
                    _textures.Add(CurveToolTextures.OptionGizmo, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_OptionGizmo.png"));
                return _textures[CurveToolTextures.OptionGizmo];
            }
        }
        public static Texture2D UIRES_CurveToolOptionPoints
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.OptionPoints))
                    _textures.Add(CurveToolTextures.OptionPoints, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_OptionPoints.png"));
                return _textures[CurveToolTextures.OptionPoints];
            }
        }
        public static Texture2D UIRES_CurveToolOptionTangents
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.OptionTangents))
                    _textures.Add(CurveToolTextures.OptionTangents, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_OptionTangents.png"));
                return _textures[CurveToolTextures.OptionTangents];
            }
        }

        public static Texture2D UIRES_CurveToolOptionEmpty
        {
            get
            {
                if (!_textures.ContainsKey(CurveToolTextures.Empty))
                    _textures.Add(CurveToolTextures.Empty, LoadAssetAuto<Texture2D>(_basePath + SubPath_UI + "/UIRES_CurveTool_Empty.png"));
                return _textures[CurveToolTextures.Empty];
            }
        }

        // GUIContent objects
        private static GUIContent _guicont_CurveTool = null;
        public static GUIContent GUICONT_CurveToolCursor
        {
            get
            {
                if (_guicont_CurveTool == null)
                    _guicont_CurveTool = new GUIContent(UIRES_CurveToolCursor, "Edit the curve inside the scene view.");
                return _guicont_CurveTool;
            }
        }
        private static GUIContent _guicont_CurveToolAdd = null;
        public static GUIContent GUICONT_CurveToolAdd
        {
            get
            {
                if (_guicont_CurveToolAdd == null)
                    _guicont_CurveToolAdd = new GUIContent(UIRES_CurveToolAdd, "Add control points to the curve");
                return _guicont_CurveToolAdd;
            }
        }
        private static GUIContent _guicont_CurveToolRemove = null;
        public static GUIContent GUICONT_CurveToolRemove
        {
            get
            {
                if (_guicont_CurveToolRemove == null)
                    _guicont_CurveToolRemove = new GUIContent(UIRES_CurveToolRemove, "Add control points to the curve");
                return _guicont_CurveToolRemove;
            }
        }
        private static GUIContent _guicont_CurveToolCorner = null;
        public static GUIContent GUICONT_CurveToolCorner
        {
            get
            {
                if (_guicont_CurveToolCorner == null)
                    _guicont_CurveToolCorner = new GUIContent(UIRES_CurveToolCorner, "Make control point a corner");
                return _guicont_CurveToolCorner;
            }
        }
        private static GUIContent _guicont_CurveToolSmooth = null;
        public static GUIContent GUICONT_CurveToolSmooth
        {
            get
            {
                if (_guicont_CurveToolSmooth == null)
                    _guicont_CurveToolSmooth = new GUIContent(UIRES_CurveToolSmooth, "Make control point smooth");
                return _guicont_CurveToolSmooth;
            }
        }
        private static GUIContent _guicont_CurveToolPivot = null;
        public static GUIContent GUICONT_CurveToolPivot
        {
            get
            {
                if (_guicont_CurveToolPivot == null)
                    _guicont_CurveToolPivot = new GUIContent(UIRES_CurveToolPivot, "Edit the curve's pivot position");
                return _guicont_CurveToolPivot;
            }
        }


        private static GUIContent _guicont_CurveToolPencil = null;
        public static GUIContent GUICONT_CurveToolPencil
        {
            get
            {
                if (_guicont_CurveToolPencil == null)
                    _guicont_CurveToolPencil = new GUIContent(UIRES_CurveToolPencil, "Draw the curve's points from scratch");
                return _guicont_CurveToolPencil;
            }
        }
        private static GUIContent _guicont_CurveToolPencilAdd = null;
        public static GUIContent GUICONT_CurveToolPencilAdd
        {
            get
            {
                if (_guicont_CurveToolPencilAdd == null)
                    _guicont_CurveToolPencilAdd = new GUIContent(UIRES_CurveToolPencilAdd, "Append point to the curve's end");
                return _guicont_CurveToolPencilAdd;
            }
        }
        private static GUIContent _guicont_CurveToolPencilCancel = null;
        public static GUIContent GUICONT_CurveToolPencilCancel
        {
            get
            {
                if (_guicont_CurveToolPencilCancel == null)
                    _guicont_CurveToolPencilCancel = new GUIContent(UIRES_CurveToolPencilCancel, "End viewport creation of curve points");
                return _guicont_CurveToolPencilCancel;
            }
        }
        private static GUIContent _guicont_CurveToolPencilRemove = null;
        public static GUIContent GUICONT_CurveToolPencilRemove
        {
            get
            {
                if (_guicont_CurveToolPencilRemove == null)
                    _guicont_CurveToolPencilRemove = new GUIContent(UIRES_CurveToolPencilRemove, "Remove drawn curve point");
                return _guicont_CurveToolPencilRemove;
            }
        }


        private static GUIContent _guicont_CurveToolOptionGizmo = null;
        public static GUIContent GUICONT_CurveToolOptionGizmo
        {
            get
            {
                if (_guicont_CurveToolOptionGizmo == null)
                    _guicont_CurveToolOptionGizmo = new GUIContent(UIRES_CurveToolOptionGizmo, "Display gizmos on curve types");
                return _guicont_CurveToolOptionGizmo;
            }
        }
        private static GUIContent _guicont_CurveToolOptionPoints = null;
        public static GUIContent GUICONT_CurveToolOptionPoints
        {
            get
            {
                if (_guicont_CurveToolOptionPoints == null)
                    _guicont_CurveToolOptionPoints = new GUIContent(UIRES_CurveToolOptionPoints, "Toggle free/directed curve point handles");
                return _guicont_CurveToolOptionPoints;
            }
        }
        private static GUIContent _guicont_CurveToolOptionTangents = null;
        public static GUIContent GUICONT_CurveToolOptionTangents
        {
            get
            {
                if (_guicont_CurveToolOptionTangents == null)
                    _guicont_CurveToolOptionTangents = new GUIContent(UIRES_CurveToolOptionTangents, "Toggle free/directed curve tangent handles");
                return _guicont_CurveToolOptionTangents;
            }
        }

        private static GUIContent _guicont_CurveToolOptionHelp = null;
        public static GUIContent GUICONT_CurveToolOptionHelp
        {
            get
            {
                if (_guicont_CurveToolOptionHelp == null)
                    _guicont_CurveToolOptionHelp = new GUIContent(UIRES_CurveToolHelp, "Show/Hide curve tools help");
                return _guicont_CurveToolOptionHelp;
            }
        }

    }
#endif
}